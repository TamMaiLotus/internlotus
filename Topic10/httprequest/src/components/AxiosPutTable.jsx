import React, { useState } from 'react'

export const AxiosPutTable = ({ putData, task }) => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        if (title && body) {
            putData([title, body], task.id);
        }
    };
    return (
        <tr>
            <td>
                <form onSubmit={handleSubmit} className="App">
                    <label>Title</label>
                    <input
                        type="text"
                        value={title}
                        onChange={(e) =>
                            setTitle(e.target.value)}
                        className="todo-input"
                        placeholder='Put title here'
                    />
                    <label>Body</label>
                    <input
                        type="text"
                        value={body}
                        onChange={(e) =>
                            setBody(e.target.value)}
                        className="todo-input"
                        placeholder='Put body here'
                    />
                    <button type='submit' className='todo-btn'>Edit Task</button>
                </form>
            </td>
        </tr>
    )
}