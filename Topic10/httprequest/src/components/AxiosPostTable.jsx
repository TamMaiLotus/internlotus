import React, { useState } from 'react'

export const AxiosPostTable = ({ postData, task }) => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');

    const handleSubmit = (e) => {
        // prevent default action
        e.preventDefault();
        if (title && body) {
            // add todo
            postData([title, body], task.id);
            // clear form after submission
            setTitle('');
            setBody('');
        }
    };
    return (
        <form onSubmit={handleSubmit} className="App">
            <label>Title</label>
            <input 
                type="text" 
                value={title} 
                onChange={(e) => 
                    setTitle(e.target.value)}
                className="todo-input" 
                placeholder='Put title here'
            />
            <label>Body</label>
            <input 
                type="text" 
                value={body} 
                onChange={(e) => 
                    setBody(e.target.value)} 
                className="todo-input" 
                placeholder='Put body here'
            />
            <button type='submit' className='todo-btn'>Add Task</button>
        </form>
    )
}