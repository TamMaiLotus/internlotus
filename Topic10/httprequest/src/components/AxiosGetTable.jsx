import React from 'react'

export const AxiosGetTable = ({ props, deleteData, editData }) => {
    console.log(props);
    return (
        <tr>
            <td className='border-style'>{props.id}</td>
            <td className='border-style'>{props.title}</td>
            <td className='border-style'>{props.body}</td>
            <td className='border-style'>
                <button onClick={() => deleteData(props.id)}>Delete</button>
            </td><td className='border-style'>
                <button onClick={() => editData(props.id)}>Edit</button>
            </td>
        </tr>
    )
}