import React, { useState, useEffect } from 'react';
import Axios from 'axios'
import { AxiosGetTable } from './axiosGetTable';
import { AxiosPostTable } from './AxiosPostTable';
import { AxiosPutTable } from './AxiosPutTable';

export const AxiosWrapper = () => {
    const [data, setData] = useState([])

    useEffect(() => {
        Axios
            .get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                console.log("Getting from :::: ", res.data)
                setData(res.data)
            })
            .catch(err => console.log(err))
        }, []   
    );

    const postData = ([title, body]) => {
        Axios
            .post('https://jsonplaceholder.typicode.com/posts', {
                title,
                body
            })
            .then (res => {
                console.log('Posting data', res)
                setData([
                    ...data,
                    {title: title, body: body, isEditing: false}
                ])
            })
            .catch(err => console.log(err));
        
    }
    
    const deleteData = (id) => {
        console.log('delete function called');
        Axios
            .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
            .then (res => {
                console.log('Deleting data', res)
                setData(data.filter(dat => dat.id !== id))
            })
            .catch(err => console.log(err));

    }

    const editData = id => {
        setData(data.map(dat => dat.id === id
            ? {
                ...dat,
                isEditing: !dat.isEditing
            } : 
                dat
            ))
        console.log(data.isEditing);
    }

    const putTask = ([title, body], id) => {
        console.log('put function called');
        Axios
            .put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
                title,
                body,
            })
            .then (res => {
                console.log('Edit data', res)
                setData(data.map(dat => dat.id === id
                    ? {
                        ...dat,
                        title,
                        body,
                        isEditing: !dat.isEditing
                    } : 
                        dat
                    ))
                })
            .catch (err => console.log(err));
    }

    return (
        <div className="App">
          <h1>Axios with ReactJS</h1>
          <AxiosPostTable postData={postData} />
          <table>
            <tr>
                <th className='border-style'>ID</th>
                <th className='border-style'>Title</th>
                <th className='border-style'>Body</th>
            </tr>
            {data.map((data) => (
                !data.isEditing ? (
                    <AxiosGetTable props={data}
                        deleteData={deleteData}
                        editData={editData}
                    />
                ) : (
                    <AxiosPutTable putData={putTask} task={data} />
                )
            ))}
          </table>
        </div>
    )
}