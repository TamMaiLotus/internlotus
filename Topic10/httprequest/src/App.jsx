import { useState } from 'react'
import './App.css'

import { AxiosWrapper } from './components/axiosWrapper'

function App() {
  return (
    <>
      <AxiosWrapper />
    </>
  )
}

export default App
