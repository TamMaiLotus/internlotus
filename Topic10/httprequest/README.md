# HTTP Client
## Rest API

**API** (Application Program Interface) 
* là một giao diện mà một hệ thống máy tính hay ứng dụng cung cấp để cho phép các yêu cầu dịch vụ có thể được tạo ra từ các chương trình máy tính khác, và/hoặc cho phép dữ liệu có thể được trao đổi qua lại giữa chúng.
* là giao diện kết nối giữa front-end và back-end. API cho phép front end gửi yêu cầu đến back end và nhận dữ liệu trả về.
* Tuân thủ theo chuẩn REST và HTTP
* Thường ứng dụng vào website (kết nối, lấy dữ liệu hoặc cập nhật cơ sở dữ liệu), hệ điều hành, thư viện hay framework phần mềm (mô tả và quy định các hành động mong muốn mà thư viện cung cấp)
* Trả về dữ liệu dưới dạng kiểu dữ liệu phổ biến như JSON hoặc XML

**API key**: 
* Là một dãy string code được truyền tải bởi API để xác định chương trình, dev, hoặc người dùng nó tới trang web. Mục đích nhằm giới hạn, kiểm soát sử dụng API.
* Thường hoạt động như một mã định danh duy nhất và mã thông báo bí mật để xác thực, và thường sẽ có một bộ quyền truy cập API được liên kết với nó. 
* Có thể dựa trên hệ thống định danh duy nhất toàn cầu (UUID) để đảm bảo duy nhất

**Lí do sử dụng API**:
* Tối ưu hoá việc chia sẻ tài nguyên và thông tin
* Kiểm soát người dùng truy cập và quyền hạn thông qua xác thực và xác định quyền hạn
* Đảm bảo được kiểm soát và an toàn
* Không cần hiểu rõ chi tiết phần mềm đang lấy tài nguyên về
* Giao tiếp đồng nhất giữa các dịch vụ kể cả bất đồng công nghệ.

**REST** (Representational State Transfer): là một dạng chuyển đổi cấu trúc dữ liệu, kiến trúc để viết API cho các dịch vụ web. Nó sử dụng phương thức HTTP đơn giản để tạo cho giao tiếp giữa các máy. Vì vậy, thay vì sử dụng một URL cho việc xử lý một số thông tin người dùng, REST gửi một yêu cầu HTTP như GET, POST, DELETE, vv đến một URL để xử lý dữ liệu.

**RESTful API** là một tiêu chuẩn dùng trong việc thiết kế các API cho các ứng dụng web. Nó cung cấp các giao diện đơn giản, đồng nhất để tạo và quản lí các tài nguyên có trên các web URL. Nhằm cung cấp các dịch vụ web service cho ứng dụng, giao tiếp giữa backend và frontend thông qua phương thức http get put post delete

6 nguyên tắc sau đây phải được đảm bảo để cho các dịch vụ API được RESTful:

* Dùng giao diện đồng nhất: Áp đặt nhiều ràng buộc kiến trúc để hướng dẫn hành vi cho các thành phần. Ngoài ra, các tài nguyên nên đặc trưng để dễ dàng nhận diện thông qua một URL duy nhất.
* Client-server based (Dựa trên kiến trúc client-server): Giao diện đồng nhất tách biệt người dùng khỏi các vấn đề dữ liệu. Phía người dùng liên quan đến UI và việc thu thập yêu cầu, trong khi phía server tập trung vào việc truy cập dữ liệu, quản lý công việc và bảo mật. **Sự tách biệt giúp mỗi phần có thể được phát triển và cải tiến độc lập với nhau**.
* Stateless operation: Yêu cầu từ máy khách đến máy chủ phải chứa các thông tin cần thiết để máy chủ có thể hiểu và xử lí phù hợp. Và phía máy chủ không lưu giữ trạng thái hay thông tin nào của máy khách mà chỉ trả về dữ liệu mà máy khách cần.
* Lưu trữ tài nguyên RESTful: Các dữ liệu trong một phản hồi cho một yêu cầu nào đó phải được đánh dấu là có thể hay ko thể lưu trữ.
* Hệ thống phân lớp: REST cho phép một kiến trúc được tạo thành từ các lớp phân cấp. Tức, mỗi component không thể nhìn thấy lớp nào xa hơn lớp nó tương tác.

**Nguyên lí hoạt động của REST API**
(REST dùng một tài nguyên định danh để nhận biết một tài nguyên cụ thể trong giao thức giữa các component.)

REST hoạt động chủ yếu dựa vào giao thức HTTP. Các hoạt động cơ bản nêu trên sẽ sử dụng những phương thức HTTP riêng.

* GET (READ): Trả về một Resource hoặc một danh sách Resource.
* POST (CREATE): Tạo mới một Resource.
* PUT (UPDATE): Cập nhật thông tin cho Resource.
* DELETE (DELETE): Xoá một Resource.

Những phương thức này thường được gọi là CRUD - Create, Read, Update, Delete.

Ngoài ra, có 5 method khác: HEAD, PATCH, CONNECT, OPTIONS, TRACE 



## Declare Props as interface.