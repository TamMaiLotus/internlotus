# SWR
(stale-while-revalidate)

## Khái niệm:
SWR là 1 thư viện cho phép vô hiệu hoá bộ nhớ cache HTTP phổ biến được thúc đẩy bởi HTTP RFC 5861, gồm các bước:
* Đầu tiên, trả về data từ bộ nhớ cache (stale - cũ).
* Sau đó, gửi yêu cầu truy xuất (revalidate - xác nhận lại).
* Cuối cùng, đưa ra dữ liệu mới nhất
Với SWR, các components sẽ luôn nhận được 1 luồng dữ liệu được cập nhật liên tục và tự động. Từ đó, giúp giao diện người dùng sẽ luôn được hồi đáp một cách nhanh chóng.

## Cấu trúc:
```
const { data, error, isLoading, isValidating, mutate } = useSWR(key, fetcher, options)
```
**Parameters**
* ``key``: 1 key riêng biệt cho request (hoặc là function/array/null) (details)
* ``fetcher``: (optional) một hàm return để fetch data
* ``options``: (optional) một object về các option cho hook SWR này

**Giá trị trả về**
* ``data``: data cho một key giải quyết bởi fetcher
* ``error``: Error gọi bởi fetcher (hoặc undefined)
* ``isLoading``: True nếu có một request đang diễn ra và không có "loaded data". Fallback data và previous data ko tính.
* ``isValidating``: True nếu có một request hay một re-validation đang được load.
* ``mutate(data, option)``: Hàm để thay đổi cached data

## Lợi ích:
**Tránh lặp request khi Fetch data**
Xét về việc sử dụng useEffect hook để fetch API data về
```
const Home = () => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        fetch('api/user')
            .then(res => res.json())
            .then(data => setUser(data));
    }, []);
}
```
![fetch-data-1](img/img1.png)
Với đoạn code trên, đoạn code sẽ gửi 2 lần request lên API endpoints thông qua useEffect(). Điều này sẽ được fix thông qua SWR hook với cách sau:
* B1: Tạo một fetcher cho hook:
```
const fetcher = (url: string) => fetch(url).then(res => res.json());
```
Đoạn fetcher trên sẽ gửi một fetch request tới url được cung cấp và truyền một response dưới dạng json.
* B2: Khởi tạo useSWR hook:
```
const { data: user, error } = useSWR('api/user', fetcher);
```
Với việc sử dụng hook trên, các bước khởi tạo state hay gán component vào mount đều được trừu tượng hoá. Hook này nhận một đối số là một API endpoint, và truyền qua một đối số khác là hàm ``fetcher``. Trong khi fetch request này chưa được thực hiện hay hoàn thành, object ``data`` được trả về dưới dạng ``undefined``, và ngược lại, object ``data`` được trả về dưới dạng đối số và lưu nó vào biến ``user``.
![fetch-data-2](img/img2.png)
Chúng ta có thể thấy rằng chỉ có 1 request được gửi lên để load data này.

**Sử dụng fetcher globally:**

SWR sử dụng một Context gọi là ``SWRconfig`` để lấy một hàm fetcher và share nó với mọi component trong app.

App.js:
```
//* Imports
import React from "react";
import { SWRConfig } from "swr";
import axios from "axios";
import Users from "./Users";

//* Set Axios Base URL
axios.defaults.baseURL = "https://jsonplaceholder.typicode.com/";

function App() {
  const fetcher = (url) => axios.get(url).then((res) => res.data);

  return (
    <SWRConfig
      value={{
        fetcher,
      }}
    >
      <Users />
    </SWRConfig>
  );
}

export default App;
```

Tạo một fetcher, sau đó sử dụng thẻ ``SWRConfig`` để đưa fetcher đó wrap xung quanh các component con ở render. Từ đó, mọi component con đều có thể sử dụng fetcher này.

```
//* Imports
import useSWR from "swr";

function Users() {
  const { data: users, error } = useSWR("/users");

  if (error) return <h1>Error!</h1>;
  if (!users) return <h1>Loading...</h1>;

  return (
    <div>
      <h1>Users</h1>
      {users.map((user) => {
        return <h2>{user.name}</h2>;
      })}
    </div>
  );
}

export default Users;
```
Ngoài ra, trong hệ thống Nesting của SWRConfig, value sẽ nhận từ value SWRConfig từ đứa cha, gộp vào và thay đổi data đấy với value nhập vào của đứa con:
```
import { SWRConfig, useSWRConfig } from 'swr'
 
function App() {
  return (
    <SWRConfig
      value={{
        dedupingInterval: 100,
        refreshInterval: 100,
        fallback: { a: 1, b: 1 },
      }}
    >
      <SWRConfig
        value={{
          dedupingInterval: 200, // will override the parent value since the value is primitive
          fallback: { a: 2, c: 2 }, // will merge with the parent value since the value is a mergeable object
        }}
      >
        <Page />
      </SWRConfig>
    </SWRConfig>
  )
}
 
function Page() {
  const config = useSWRConfig()
  // {
  //   dedupingInterval: 200,
  //   refreshInterval: 100,
  //   fallback: { a: 2,  b: 1, c: 2 },
  // }
}
```

**Auto Revalidation**
SWR cung cấp khả năng revalidate data với rất nhiều trường hợp, thường hay được dùng khi refresh data với các tab ngủ đông, hay với các tab có data giống nhau. Thay đổi cách thức revalidate ở parameter ``option``, gồm:
* Focus Revalidation
* Refetch on Interval
* Revalidate on Reconnect

**Dependent Fetching**
SWR cho phép bạn tìm dữ liệu phụ thuộc vào một dữ liệu khác. Giống như là bạn get id của một user sau đó dựa vào userId ấy để tìm ra các project tương ứng của user.
```
function MyProjects() {
  const { data: user } = useSWR('/api/user')
  const { data: projects } = useSWR(() => '/api/projects?uid=' + user.id)

  // When passing a function, SWR will use the return
  // value as `key`. If the function throws or returns
  // falsy, SWR will know that some dependencies are not
  // ready. In this case `user.id` throws when `user`
  // isn't loaded.

  if (!projects) return 'loading...'
  return 'You have ' + projects.length + ' projects'
}

```
**Pagination**
Cách khởi tạo để phân trang:
```
function Page ({ index }) {
  const { data } = useSWR(`/api/data?page=${index}`, fetcher);
 
  // ... handle loading and error states
 
  return data.map(item => <div key={item.id}>{item.name}</div>)
}
 
function App () {
  const [pageIndex, setPageIndex] = useState(0);
 
  return <div>

    // Insert your content here

    <Page index={pageIndex}/>
    // this line is for preloading the next page
     <div style={{ display: 'none' }}><Page index={pageIndex + 1}/></div>
    <button onClick={() => setPageIndex(pageIndex - 1)}>Previous</button>
    <button onClick={() => setPageIndex(pageIndex + 1)}>Next</button>
  </div>
}
```

**Data Mutation**
Bound Mutation dùng để biến đổi key hiện tại với dữ liệu mới. Khoá được ràng buộc với khoá truyền vào bởi useSWR, và nhận dữ liệu, là đối số đầu tiên.
```
import useSWR from 'swr'
 
function Profile () {
  const { data, mutate } = useSWR('/api/user', fetcher)
 
  return (
    <div>
      <h1>My name is {data.name}.</h1>
      <button onClick={async () => {
        const newName = data.name.toUpperCase()
        // send a request to the API to update the data
        await requestUpdateUsername(newName)
        // update the local data immediately and revalidate (refetch)
        // NOTE: key is not required when using useSWR's mutate as it's pre-bound
        mutate({ ...data, name: newName })
      }}>Uppercase my name!</button>
    </div>
  )
}
```

**Custom Hook**
Bản thân SWR là một hook. Do đó có thể tuỳ ý custom hook từ hooks. Nó cho phép viết code không bị lặp. Giả sử khi fetch data từ API /users chẳng hạn, mỗi lần muốn fetch data thì cần phải truyền qua truyền lại giữa các functions. Nhưng nếu custom hook thì sẽ không cần phải truyền qua truyền lại nhiều như vậy nữa.

```
function App() {
    const { data, error, isValidating } = useSWR('/users', fetcher)
    return <>.... </>
}

// instead
function useUser() {
    return useSWR('/users', fetcher)
}

function App() {
    const { data, error, isValidating } = useUser()
    return <>.... </>
}
```