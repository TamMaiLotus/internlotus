//Transfer ERC20 token

const { ethers } = require('ethers')

const node = 'quick node'
const provider = new ethers.providers.JsonRpcProvider(node)


const tokenAddress = 'token address'
const tokenAbi = []

const privateKey = 'sender key'
const wallet = new ethers.Wallet(privateKey, provider)
const contract = new ethers.Contract(tokenAddress, tokenAbi, wallet)

const receiver = 'receiver address'
const sender = 'sender address'

const amountToSend = '1'


async function main() {
    const decimals = await contract.decimals()
    const balance = await contract.balanceOf(sender)
    //console.log(ethers.utils.formatUnits(balance, decimals))

    contract.transfer(receiver, ethers.utils.parseEther(amountToSend, decimals))
    .then(function(transaction){
        console.log(transaction)
    })

}

main()