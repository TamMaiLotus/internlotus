

const { ethers } = require('ethers')

const node = 'quick node'
const provider = new ethers.providers.JsonRpcProvider(node)

const privateKey = 'sender key'
const wallet = new ethers.Wallet(privateKey, provider)

const receiver = 'receiver address'
const sender = 'sender address'

const amountToSend = '0.00045'

const transaction = {
    to: receiver,
    value: ethers.utils.parseEther(amountToSend)
}

async function main() {
    // const balance = await provider.getBalance(sender)
    // console.log(ethers.utils.formatEther(balance))

    wallet.sendTransaction(transaction)
        .then((transactionObj) => {
            console.log('transactionHash', transactionObj.hash)
    })
}
main()