//SPDX-License-Identifier: MIT
pragma solidity >=0.8.0;

import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

contract Billion is ERC20PresetMinterPauser {
    constructor() ERC20PresetMinterPauser("TokenCoin", "TOKEN") {
        mint(msg.sender, 1_000_000_010 * 10**18);
    }
}