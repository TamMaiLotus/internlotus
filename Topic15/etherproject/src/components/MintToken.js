import { ethers } from "ethers";

let isInitialized = false;

export const mintToken = async (nftContract, defaultAccount) => {
    console.log('nftContact: ', nftContract)
    console.log('defaultAccount: ', defaultAccount)
    if (!isInitialized) {
        await init();
    }
    const mintFunction = nftContract.interface.functions.mint;
    const tx = await defaultAccount.sendTransaction({
        to: nftContract.address,
        data: mintFunction.encode([defaultAccount]),
    })
    return tx.wait();
}