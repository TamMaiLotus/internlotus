import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import { ethers } from 'ethers';
import NFTContractBuild from '../../truffle/build/contracts/NFT.json'
import { mintToken } from './MintToken';

const provider = new ethers.providers.Web3Provider(window.ethereum, "any")
console.log("Provider: ", provider)

const privateKey = '0x33f0750086c316ea550f8dac5a075d883503be743a4e1c761e080200572a558e'
const wallet = new ethers.Wallet(privateKey, provider)

const amountToSend = '0.1'

const WalletCard = () => {
    const [errorMessage, setErrorMessage] = useState(null);
    const [defaultAccount, setDefaultAccount] = useState(null);
    const [userBalance, setUserBalance] = useState(null);

    const nftContract = new ethers.Contract(
        '0xEBe3882bA004C7d02c1160127C44AeBE2BdcB624',
        NFTContractBuild.abi,
        provider.getSigner(),
    )
    console.log("NFTContract", nftContract);

    const connectWalletHandler = () => {
        if (window.ethereum) {
            provider.send("eth_requestAccounts", []).then(async (accounts) => {
                console.log("Current account: ", accounts[0])
                await accountChangedHandler(provider.getSigner());
            })
        } else {
            setErrorMessage("Please Install Metamask!!!");
        }
    }
    const accountChangedHandler = async (newAccount) => {
        const address = await newAccount.getAddress();
        setDefaultAccount(address);
        const balance = await newAccount.getBalance()
        setUserBalance(ethers.utils.formatEther(balance));
        await getUserBalance(address)
    }
    const getUserBalance = async (address) => {
        const balance = await provider.getBalance(address, "latest")
    }
    const transaction = {
        to: defaultAccount,
        value: ethers.utils.parseEther(amountToSend)
    }
    const handleTransaction = () => {
        wallet.sendTransaction(transaction)
            .then((transactionObj) => {
                console.log('transactionHash', transactionObj.hash)
            })
    }

    return (
        <div className="WalletCard">
            <h3 className="h4">
                Welcome to a decentralized Application
            </h3>
            <Button
                style={{ background: defaultAccount ? "#A5CC82" : "white" }}
                onClick={connectWalletHandler}>
                {defaultAccount ? "Connected!!" : "Connect"}
            </Button>
            <div className="displayAccount">
                <h4 className="walletAddress">Address:{defaultAccount}</h4>
                <div className="balanceDisplay">
                    <h3>
                        Wallet Amount: {userBalance}
                    </h3>
                </div>
            </div>
            <Button
                style={{ background: "white" }}
                onClick={handleTransaction}>Get some Token to Wallet</Button>
            {errorMessage}
        </div>
    )
}
export default WalletCard;

