import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import WalletCard from './components/WalletCard'
import './App.css'

function App() {
  return (
    <div className="App-header">
      <div className="centerCard">
        <div className = "card">
          <div className="App">
            <WalletCard/>
          </div>
        </div>
      </div>
    </div>
  );
}
export default App;