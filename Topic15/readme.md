# Testnet
## 1. Khái niệm:
* Testnet là 1 blockchain được tạo ra để cho việc kiểm thử. 
* Nó là một mạng lưới testing cho việc check phần code chạy ổn định hay ko trước khi mang nó vào mainnet (giao thức nâng cấp, các giao thức SC)
* Đa số testnet sử dụng cơ chế đồng thuận PoA (cược theo danh tiếng), còn một số thì sử dụng PoS như mainnet

## 2. Goerli
* Là một testnet dùng cho việc kiểm thử xác nhận và đặt cược. 
* dùng cho những người đặt cược mong muốn kiểm thử các giao thức nâng cấp trước khi deploy sang mainnet
  * Bộ xác thực mở cho stakers để kiểm thử network upgrades
  * Large state cho việc kiểm thử tương tác SC phức tạp
* Việc đồng bộ dài hơn và cần thêm bộ nhớ để chạy một node

# Etherum development standard
Cộng đồng Ethereum đã áp dụng nhiều tiêu chuẩn giúp duy trì tính tương tác của các dự án (như các client và ví) trên các phiên bản khác nhau, đảm bảo các hợp đồng thông minh và ứng dụng phi tập trung (dapps) vẫn có thể kết hợp được.
## Ethereum Imporvement Proposals (EIPs)
Là các đề xuất cụ thể về các tính năng hoặc quy trình mới cho Ethereum. EIPs chứa các thông số kỹ thuật về những thay đổi được đề xuất và đóng vai trò như "nguồn thông tin chính" cho cộng đồng. Các nâng cấp mạng và tiêu chuẩn ứng dụng cho Ethereum được thảo luận và phát triển thông qua quá trình EIP.
Có 3 loại EIPs:
* Standards Track: mô tả các thay đổi ảnh hưởng đến đa số hoặc tất cả các phiên bản Ethereum
  * Core: các cải tiến yêu cầu sự đồng thuận
  * Networking: Các cải tiến liên quan đến devp2p và giao thức Ethereum nhẹ, cũng như các đề xuất cho việc cải tiến các thông số kĩ thuật giao thức mạng cho whisper và swarm
  * Interfacce: Các cải tiến liên quan đến các thông số kĩ thuật và tiêu chuẩn API/RPC của client, và một số tiêu chuẩn cấp ngôn ngữ như là tên phương thức hay các hợp động ABIs.
  * ERC: Các tiêu chuẩn và quy ước cấp ứng dụng
* Meta Track: Mô tả các quá trình xảy ra xung quanh Ethereum hoặc đề xuất thay đổi cho một quá trình
* Informational Track: Mô tả vấn đề trong bản thể Ethereum hoặc cung cấp hướng dẫn hoặc thông tin đến Ethereum community

## 2. Token Standard (ether, token, native token)
Chức năng:
  *  Chuyển token từ account này sang account khác
  *  Lấy token balance hiện tại của một tài khoản
  *  Lấy tổng cung cấp Token có sẵn trên mạng
  *  Xác định xem một lượng Token có thể được chi tiêu bởi một bên thứ ba hay không
* ERC-20: Là tiêu chuẩn cho Token có thể thay thế. Tức mỗi Token sẽ có thuộc tính khiến cho các Token này sẽ giống nhau (về loại và giá trị) so với các Token khác, thì giá trị của nó sẽ luôn như nhau
https://ethereum.org/en/developers/docs/standards/tokens/erc-20/
* ERC-721: Là tiêu chuẩn cho Token không thể thay thế (NFT). Tức mỗi Token đều đặc trưng và có thể có các value khác nhau so với các Token khác trong cùng một SC. Mỗi token sẽ có 1 ID, ID này chứa tokenURI dẫn đến 1 metadata gồm tên, mô tả, url image, và các thuộc tính của nó
* ERC-1155: Là 721 + 20, tức quản lí nhiều token giống nhau ở một mảng token. Mỗi token này sẽ gồm một ID, địa chỉ - ai sở hữu nó, và địa chỉ đó chứa bao nhiêu lượng token đấy

# NFT
## 1. Khái niệm
Một NFT sẽ bao gồm tài sản (Token), có thể đại diện cho những gì ảo trên Ethereum:
* Điểm danh tiếng trên nền tảng online
* Vé số
* Tiền tệ
* ounce of gold
* ...
; một thông tin về tài sản (metadata); và một SC chứa thông tin về metadata

# Ethersjs
## Provider
Là sự trừu tượng hoá cho connection tới một ethereum network, cung cấp giao diện ngắn gọn, nhất quán cho các tiêu chuẩn chức năng của Ethereum node
`provider.getBalance(address)`: trả về balance của một wallet có address là `address`
`provider.getTransactionCount(address)`: trả về số transaction mà ví address này từng gửi
`provider.ready`: (?) stall cho đến khi network được thiết lập, bỏ qua các lỗi do các target node chưa dc active. Dùng cho việc testing hoặc gắn scripts để đợi đến khi node chạy được smoothly
`provider.getTransaction(hash)`: trả về transaction với mã hash hoặc null nếu transaction is unknown
`provider.sendTransaction(to, value)`: submit transaction lên network để được mine. Transaction này phải được kí, và hợp lí (đúng nonce và tài khoản có đủ balance để mà trả cho transaction đấy)
## JsonRpcProvider
Là method để tương tác với Ethereum và có sẵn trên các Ethereum node implementations (Geth, Parity) và các services bên thứ 3 (Infura)
``new ethers.providers.JsonRpcProvider(`URLOrConnetionInfo`)``: Kết nối tới JSON-RPC HTTP API thông qua URL hoặc ConnectionInfo của network đó
`jsonRpcProvider.getSigner([addressOrIndex])`: Trả về một signer được quản lí bởi node Ethereum này, tại address hoặc index được gọi tới. Mặc định sẽ gọi account#0 nếu không có gì được cung cấp
`jsonRpcProvider.listAccounts()`: Trả về danh sách các account được quản lí bởi provider này
## TransactionRequest
Mô tả transaction được gửi lên network hoặc là qua xử lí, bao gồm các thành phần `.to`, `.from`, `.nonce`, `.data`, `.value` (dưới dạng wei), ...
## TransactionResponse
Bao gồm các props của một transaction một khi đã được mine
`transaction.wait(1)`: Đợi cho transaction được mine (?)
## Signer
Là một trừu tượng hoá cho một Ethereum Account, được dùng cho việc kí các tin nhắn và các transactions và gửi các transactions đã kí lên Ethereum network để chạy các toán tử state-changing.
`signer.getAddress()`: trả về một Promise cho address của signer
(...)
## Wallet
Kế thừa Signer và có thể kí transactions và messages sử dụng một private key.
`new ethers.Wallet(privateKey, provider)`: Tạo một Wallet mới sử dụng một private key và connect nó tới một provider
`ethers.Wallet.createRandom()`: Tạo một Wallet random
## Contract
Là một ảo hoá của đoạn code đã được deploy lên blockchain. Một contract có thể gửi transaction

(Note thêm: Differences between signTransaction and sendTransaction)


