const { ethers } = require("ethers");

const INFURA_ID = '905480cf9f84454082993ce65a2fb67c'
const provider = new ethers.providers.JsonRpcProvider(`https://sepolia.infura.io/v3/${INFURA_ID}`)

const account1 = '0xBF4bDF4249F93Dff251DFC6b145a7b1b27B1b024' // sender
const account2 = '0x71a1eF803454DFf501DcF163cF4bfAa677BEBB48' // receiver

const privateKey1 = '2d7ea6e3a1703bf21f3f691f21b22ba6c92098f3092f269252993cedbfaef41d' // Private key of account 1
//creating a wallet that can be signed through code so it can do metamask-like functions
//private key can generate address
const wallet = new ethers.Wallet(privateKey1, provider)

//How cryptography works: whenever the user click on 'confirm' button on each transaction in metamask, it means that you are 'signing the transaction'. As long as you have the private key, you can sign on any transaction you want. Transaction has to be signed in order for it to be valid

const main = async () => {
    const senderBalanceBefore = await provider.getBalance(account1)
    const recieverBalanceBefore = await provider.getBalance(account2)

    console.log(`\nSender balance before: ${ethers.utils.formatEther(senderBalanceBefore)}`)
    console.log(`Reciever balance before: ${ethers.utils.formatEther(recieverBalanceBefore)}\n`)

    //send ether
    const tx = await wallet.sendTransaction({
        to: account2,
        value: ethers.utils.parseEther("0.025")
    })
    console.log(ethers.utils.parseEther("0.025"))
    //waiting for the transaction to be mined (the transaction to be marked as completed)
    await tx.wait(1)
    console.log(tx)

    const senderBalanceAfter = await provider.getBalance(account1)
    const recieverBalanceAfter = await provider.getBalance(account2)

    console.log(`\nSender balance after: ${ethers.utils.formatEther(senderBalanceAfter)}`)
    console.log(`Reciever balance after: ${ethers.utils.formatEther(recieverBalanceAfter)}\n`)
}

main()