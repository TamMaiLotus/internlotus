const { ethers } = require("ethers");

const INFURA_ID = '905480cf9f84454082993ce65a2fb67c'
const provider = new ethers.providers.JsonRpcProvider(`https://sepolia.infura.io/v3/${INFURA_ID}`)

const address = '0xBF4bDF4249F93Dff251DFC6b145a7b1b27B1b024'

const main = async () => {
    const balance = await provider.getBalance(address)
    //ethers.utils.formatEther(balance): biến đổi balance được in ra dưới dạng decimal (note)
    console.log(`\nETH Balance of ${address} --> ${ethers.utils.formatEther(balance)} ETH\n`)
}

main()

