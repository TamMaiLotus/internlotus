//import { createStore } from 'https://cdn.skypack.dev/redux';

function createStore(reducer) {
    let state = reducer(undefined, {});
    const subscribers = [];

    return {
        getState() {
            return state;
        },
        dispatch(action) {
            state = reducer(state, action);
            subscribers.forEach(subscriber => subscriber());
        },
        subscribe(subscriber) {
            subscribers.push(subscriber);
        },
    }
}

const initState = 0;

//Reducer
function Reducer(state = initState, action) {
    switch (action.type) {
        default:
            return state;
        case 'DEPOSIT':
            return state + action.payload;
        case 'WITHDRAW':
            return state - action.payload;
    }
}

//Store
const store = createStore(Reducer);
console.log (store.getState());

//Actions
function ActionDeposit(payload) {
    return {
        type: 'DEPOSIT',
        payload
    }
}
function ActionWithdraw(payload) {
    return {
        type: 'WITHDRAW',
        payload
    }
}

//DOM events
const deposit = document.querySelector('#deposit');
const withdraw = document.querySelector('#withdraw');

//Event handler
deposit.onclick = function() {
    store.dispatch(ActionDeposit(10));
}
withdraw.onclick = function() {
    store.dispatch(ActionWithdraw(10));
}

//Listener
store.subscribe(() => {
    console.log("This one is subscribed")
    render();
})
store.subscribe(() => {
    console.log("This one is subscribed too")
    render();
})

//Render
function render() {
    const output = document.querySelector('#output');
    output.innerText = store.getState();
}

render();