import {v4 as uuidv4} from 'uuid';
uuidv4();

const initialState = {
    todos: []
};

export const addTodo = (text) => ({
    type: 'todoAdded',
    text: text,
    id: uuidv4(),
});

export const toggleTodo = (id) => ({
    type: 'todoToggled',
    id: id,
})

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'todoAdded':
            console.log('List action:', action);
            return {
                ...state,
                todos: [
                    ...state.todos,
                    {
                        id: action.id,
                        text: action.text,
                        completed: false,
                        isEditing: false,
                    }
                ]
            };
        case 'todoToggled':
            return {
                ...state,
                todos: state.todos.map(todo =>
                    todo.id === action.id
                    ? {
                        ...todo,
                        completed: !todo.completed,
                    }
                    : todo,
                ),
            }
        default:
            return state;
    }
}

export default reducer;