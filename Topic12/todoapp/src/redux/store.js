import { legacy_createStore, combineReducers} from "redux";
import todoReducer from "./todo";

const reducer = combineReducers({
    todo: todoReducer
});

export default legacy_createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);