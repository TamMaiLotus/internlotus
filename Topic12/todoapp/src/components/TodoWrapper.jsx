import { connect } from 'react-redux';
import TodoApp from './todoApp';
import TodoForm from './TodoForm';
import { addTodo, toggleTodo } from '../redux/todo';

const TodoWrapper = (
    todos,
    addTodo,
    toggleTodo
) => {
    console.log('todos in TodoWrapper:', todos);
    return (
        <div>
            <TodoForm addTodo={addTodo} />
            {/* {todos.map((todo) => (
                !todo.isEditing (
                    <TodoApp task={todo}
                        toggleComplete={toggleTodo}
                    />
                )))} */}
            
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        todos: state.todo.todos
    }
}

const mapActionsToProps = {
    addTodo,
    toggleTodo
}

export default connect(mapStateToProps, mapActionsToProps)(TodoWrapper);