# Redux

## 1. Khái niệm:

React Redux là một mẫu và thư viện dùng cho việc state management trong React, giúp quản lí states cho số lượng lớn các components trong một app lớn và phức tạp.

React Redux giúp các component có thể truy cập vào state, và cho phép các component có thể thay đổi state ở bất cứ lúc nào.

## 2. Cách hoạt động

React Redux giữ trạng thái của một app ở một nơi gọi là Redux store. React component có thể lấy state mới nhất cũng như cập nhật state ở store bất cứ khi nào. Redux cung cấp quy trình đơn giản để khởi tạo và thiết lập state hiện tại cho app và bao gồm các khái niệm sau:

* **Store**: Nơi trung tâm để lưu trữ state cho app
Store được tạo thông qua việc truyền vào một reducer, và có một phương thức ``getState`` để trả về giá trị hiện tại của state đó.
```
import { configureStore } from '@reduxjs/toolkit'

const store = configureStore({ reducer: counterReducer })

console.log(store.getState())
// {value: 0}
```
* **Actions**: Là list các hoạt động, trong đó, hoạt động là một object có ``type`` field. Có thể cho rằng, action là một event miêu tả điều gì đã xảy ra trong application.

``type`` field là một string để đưa cho hành động này một cái tên, và ``payload`` field mô tả thêm hành động đó xảy ra như thế nào.
Trong đó, dòng ``domain/eventName`` với phần đầu tiên để miêu tả hành động này thuộc dạng nào, và phần tiếp theo mô tả chi tiết hành động đó.
```
const addTodoAction = {
  type: 'todos/todoAdded',
  payload: 'Buy milk'
}
```
* **Action Creators**: là một function tạo và trả về action object. Hàm này có thể gọi action object để các lần sau không cần phải viết ra nguyên action object ra mà chỉ cần gọi tên hàm.
```
const addTodo = text => {
  return {
    type: 'todos/todoAdded',
    payload: text
  }
}
```
* **Reducers**: Là một hàm nhận ``state`` hiện tại và một ``action`` object, quyết định cách để cập nhật state nếu cần thiết, và trả về state mới ``(state, action) => newState``. Có thể coi reducer là dạng event listener với việc ứng với mỗi action (event) nhận được, thì handle event tương ứng.

Reducers phải tuân theo các luật sau:
* Reducers chỉ tính toán value mới cho state dựa trên đối số ``state`` và ``action``
* Reducers ko được phép thay đổi ``state`` có sẵn. Reducers phải tạo một updates bất biến (immutable), bằng việc copy ``state`` có sẵn đó và cập nhật giá trị cho value mới copy sang.
* Reducers không được thực hiện bất kì logic ko đồng bộ nào, tính toán các giá trị ngẫu nhiên hoặc gây ra các "tác dụng phụ" khác.

Logic hoạt động của một hàm reducer:
* Kiểm tra xem reducer có quan tâm đến action này hay không.
  -> Nếu quan tâm, tạo một copy của state đó, với value mới được thêm vào, sau đó trả về state copy.
* Nếu không, trả về state đó mà không thay đổi gì cả.
```
const initialState = { value: 0 }

function counterReducer(state = initialState, action) {
  // Check to see if the reducer cares about this action
  if (action.type === 'counter/increment') {
    // If so, make a copy of `state`
    return {
      ...state,
      // and update the copy with the new value
      value: state.value + 1
    }
  }
  // otherwise return the existing state unchanged
  return state
}
```

* **Dispatch**: là một phương thức của Store, và đây là phương thức duy nhất để mà update State. Tức, phải gọi ``store.dispatch()`` và truyền vào một action object để cập nhật state. Store này sẽ chạy hàm reducer và lưu giá trị state ở trong nó, và chúng ta có thể gọi ``getState()`` để nhận về giá trị được update đó.
```
store.dispatch({ type: 'counter/increment' })

console.log(store.getState())
// {value: 1}
```
Có thể coi việc dispatch một action dưới dạng "trigger một event". Với một chuyện nào đó xảy ra, chúng ta muốn store biết đến nó. Reducers hoạt động như một event listeners, và khi chúng nghe tới một action mà chúng hứng thú, chúng sẽ update state in response.

* **Selectors**: là các hàm lấy ra một thông tin cụ thể từ giá trị của store state.
```
const selectCounterValue = state => state.value

const currentValue = selectCounterValue(store.getState())
console.log(currentValue)
// 2
```

* **Data Flow**:
![data-flow](https://d33wubrfki0l68.cloudfront.net/01cc198232551a7e180f4e9e327b5ab22d9d14e7/b33f4/assets/images/reduxdataflowdiagram-49fa8c3968371d9ef6f2a1486bd40a26.gif)
  * Khởi tạo:
    * Một Redux store sẽ được khởi tạo thông qua một hàm root reducer.
    * Store này sẽ gọi hàm root reducer 1 lần, và lưu giá trị trả về dưới dạng một ``state`` mặc định
    * Khi UI render lần đầu tiên, các component UI sẽ truy cập vào state mặc định của Redux store, và sử dụng data đấy để quyết định xem render cái gì. Chúng cũng đăng kí với các cập nhật store sắp tới để chúng biết được khi nào mà state đã thay đổi
  * Cập nhật:
    * Có một hành động đã xảy ra, ví dụ như người dùng click vào nút 'Deposit $10'
    * UI này gửi hành vi click button cho Dispatch.
    * App thông qua dispatch biến hành vi kia thành một dạng action: ``dispatch({type: 'counter/deposit'})``; và gửi action đấy sang Redux store.
    * Store sẽ chạy hàm reducer một lần nữa với previous ``state`` và current ``action``, và lưu lại giá trị trả về dưới dạng ``state`` mới.
    * Store thông báo đến tất cả phần trong UI là store đã được cập nhật.
    * Từng UI component kiểm tra xem là một phần trong state nó cần đã thay đổi hay chưa
    * Các component với dữ liệu thay đổi sẽ buộc phải re-render lại với giá trị mới, để app có thể cập nhật thứ gì xảy ra trên màn hình.