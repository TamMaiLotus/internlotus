export default function ContactList({contacts, selectedId, dispatch}) {
    function handleChangedSelection(contactId) {
      dispatch ({
        type: 'changed_selection',
        contactId: contactId, 
      });
    }
    return (
      <section className="contact-list">
        <ul>
          {contacts.map((contact) => (
            <li key={contact.id}>
              <button
                onClick={() => {
                  handleChangedSelection(contact.id);
                }}>
                {selectedId === contact.id ? <b>{contact.name}</b> : contact.name}
              </button>
            </li>
          ))}
        </ul>
      </section>
    );
  }
  