import { useState } from 'react';

export default function Chat({contact, message, dispatch}) {
  function handleEditedMessage(message){
    dispatch ({
      type: 'edited_message',
      message: message,
    })
  }
  
  return (
    <section className="chat">
      <textarea
        value={message}
        placeholder={'Chat to ' + contact.name}
        onChange={(e) => {
          handleEditedMessage(e.target.value)
        }}
      />
      <br />
      <button>Send to {contact.email}</button>
    </section>
  );
}
