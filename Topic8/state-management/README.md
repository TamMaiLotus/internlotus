# State Management:

*Đầu tiên, nếu trigger event handler ở trên các biến cục bộ, nó sẽ ko hoạt động vì:*
- Các biến cục bộ ko tồn tại giữa các render
- Đổi sang biến cục bộ sẽ ko trigger render (Túc React sẽ ko hiểu là nó đã được render với data đã dc cập nhật)

```useState``` sẽ giúp xử lí 2 vấn đề trên.
Trước tiên, thêm dòng này ở đầu code: 

```import { useState } from 'react';```

rồi thêm dòng này ở đầu của đoạn code export default:

```const [index, setIndex] = useState(0);```

## Hook 
Hook là các "chức năng" đặc biệt chỉ tồn tại khi React đang render. Điều này cho phép bạn Hook sang các chức năng khác của React

Hiểu cách khác, nó hoạt động giống như việc bạn import modules ở mỗi đầu đoạn code, tức là các khai báo không điều kiện (Không thể gọi đến trong các điều kiện, vòng lặp, hay các chức năng khác)

**State** là cô lập và riêng tư, tức nếu gọi component đấy nhiều lần, thì các component đấy đều có State riêng biệt.

- Nếu set... được goi lên, thì Reacts sẽ set value mới cho ..., đồng thời sẽ queue hành động này cho một render mới và quy tắc re-render sẽ áp dụng trong đoạn code set trong set... .
- Ở đây, value... sẽ ko thay đổi dù cho lệnh set... được gọi bao nhiêu lần cho đến lần re-render tiếp theo.
- React giữ value... fixed trong một render của event handlers

(Need a bit more research: https://react.dev/learn/state-as-a-snapshot)

(Need to add sơ đồ)

## Input với State:
Thay vì đi tới kết quả thông qua hàng loạt chỉ dẫn + yes/no, chúng ta có thể chỉ tới phần kết quả, và React sẽ tự động đi tới đó, thậm chí có thể nhanh và gọn hơn.

Khi phát triển 1 component:
- Phải khai thác hết các visual state của nó
- Quyết định trigger người dùng hay máy để cho đổi state
- Khai báo state với useState
- Loại bỏ các state ko cần thiết hoặc thừa thãi
- Kết nối với các event handler để set state

## Quy tắc tạo state:
- Group Related State: Nếu phải luôn update 2 hoặc nhiều hơn state variables cùng 1 lúc, hãy cân nhắc gộp nó lại thành 1 state variables.
Tức, thay vì: 
 ```
const [x, setX] = useState(0);
const [y, setY] = useState(0);
```
chúng ta nên: 
 ```
 const [position, setPosition] = useState({ x: 0, y: 0 });
 ```
- Tránh xung đột trong state
- Tránh dư thừa trong state: Tức, thay vì thêm một dàn state variables cùng 1 loại thông tin, chúng ta có thể khai báo một phần tử hoặc mảng chứa các variables đó trước, sau đó dùng 1 phần tử gọi chung để cho nó thành state variables.
**Lưu ý:** nếu dữ liệu khai báo trong useState() là một phần tử tham chiếu từ bên khác, thì khi cập nhật phần tử đó, bên trong useState() sẽ không cập nhật lại state mới dựa trên phần tử đã nhập. Vì vậy, nên khai báo thẳng cho phần tử nó thay vì phải khai báo thông qua state: 
thay vì:
```
 function Message({ messageColor }) {
  const [color, setColor] = useState(messageColor);
```
hãy dùng:
```
function Message({ messageColor }) {
  const [color, setColor] = useState(messageColor);
  ```

- Tránh trùng lặp trong state: Tức, đừng để giá trị trong state trùng với phần tử được gọi một cách hoàn toàn. Thay vào đó, có thể cân nhắc gọi id của phần tử đó, và khai báo id cho state.
- Tránh state lồng sâu: Hãy dùng thẳng nó, gọi id thay vì gọi cả cụm.

## Bảo tồn và reset State:
- Với mỗi component với một state, dù cho các component có giống nhau đi chăng nữa, thì nó sẽ nằm trong 2 node khác nhau dưới 1 React Tree, tức, dù khai báo giống nhau nhưng nhiều component được gọi lên đều có state hoạt động khác nhau.

![state-tree-1](https://react.dev/_next/image?url=%2Fimages%2Fdocs%2Fdiagrams%2Fpreserving_state_tree.dark.png&w=640&q=75)
![state-tree-2](https://react.dev/_next/image?url=%2Fimages%2Fdocs%2Fdiagrams%2Fpreserving_state_increment.dark.png&w=640&q=75)

- Nếu cùng 1 vị trí component, thì nếu component **giống nhau** được gọi lên 2 lần, state vẫn sẽ giữ nguyên.
VD: 
```
 {isFancy ? (
      <Counter isFancy={true} />
    ) : (
      <Counter isFancy={false} />
    )}
  ```
![preserve-state](https://react.dev/_next/image?url=%2Fimages%2Fdocs%2Fdiagrams%2Fpreserving_state_same_component.dark.png&w=640&q=75)

- Nếu cùng 1 vị trí component, thì nếu component **khác** được gọi lên, state sẽ bị reset lại, cùng với tất cả subtree của nó. Tức, component cũ ở vị trí đó sẽ bị xoá và thay thế = component mới.
VD:
```
  {isPaused ? (
        <p>See you later!</p> 
      ) : (
        <Counter /> 
      )}
  ```
![reset-state-1](https://react.dev/_next/image?url=%2Fimages%2Fdocs%2Fdiagrams%2Fpreserving_state_diff_pt1.dark.png&w=828&q=75)
![reset-state-2](https://react.dev/_next/image?url=%2Fimages%2Fdocs%2Fdiagrams%2Fpreserving_state_diff_pt2.dark.png&w=828&q=75)

* Cách reset state nếu component nằm cùng vị trí:
  * Render components ở vị trí khác nhau
  * Gán các components đó với một key riêng biệt

## Reducer:
Là một cách để xử lí state trong trường hợp có rất nhiều component với state khác nhau.

Cách chuyển sang reducer:
* B1: Chuyển đổi từ setting State sang dispatch Action.
Thay vì: 
```
function handleAddTask(text) {
  setTasks([
    ...tasks,
    {
      id: nextId++,
      text: text,
      done: false,
    },
  ]);
}
 ```
Hãy chuyển sang: 
```
function handleAddTask(text) {
  dispatch({
    type: 'added',
    id: nextId++,
    text: text,
  });
}
 ```
(Phần state update logic sẽ nằm trên phần khác)
Ở đây, thay vì gói gọn lại update Task trong 1 hàm, dispatch sẽ nói với React là người dùng mới làm gì, còn cụ thể hành động nó thế nào thì thằng khác sẽ giải thích cho.
* B2: Viết một reducer function: 
Một reducer function là nơi đặt logic của state, gồm 2 arguements - state hiện tại và action, và sẽ return ở state tiếp theo
```
function tasksReducer(tasks, action) {
  if (action.type === 'added') {
    return [
      ...tasks,
      {
        id: action.id,
        text: action.text,
        done: false,
      },
    ];
  //add more action.type here
 ```

* B3: Khai báo reducer:
```
import { useReducer } from 'react';
...
//instead of const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);

//do
const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);
 ```

 ```useReducer`` nhận 2 đối số:
  * reducer function
  * state ban đầu

và nó trả về
  * một giá trị stateful
  * một dispatch function (để gửi user actions cho reducer)

  