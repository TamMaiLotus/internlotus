import React, {useState} from 'react';
import {v4 as uuidv4} from 'uuid';
uuidv4();

import { TodoForm } from './TodoForm';
import { Todo } from './Todo';
import { EditTodoForm } from './EditTodoForm';
import Stateless from './Stateless';

export const TodoWrapper = () => {
    const [todos, setTodos] = useState([])

    const addTodo = todo => {
        setTodos([...todos, {id: uuidv4(), task: todo, completed: false, isEditing: false}])
        console.log(todos)
    }

    const toggleComplete = id => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                completed: !todo.completed
            } : 
                todo
            ))
    }

    const editTodo = id => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                isEditing: !todo.isEditing 
            } : 
                todo
            ))
    }

    const editTask = (task, id) => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                task,
                isEditing: !todo.isEditing
            } : 
                todo
            ))
    }
    const deleteTodo = id => {
        setTodos(todos.filter(todo => todo.id !== id))
    }
    return (
        <div className='TodoWrapper'>
            <h1>Get things done!</h1>
            <TodoForm addTodo={addTodo}/>
            {todos.map((todo, index) => (
                !todo.isEditing ? (
                    <Todo task={todo} key={index}
                    toggleComplete={toggleComplete}
                    deleteTodo={deleteTodo}
                    editTodo={editTodo} />
                ) : (
                    <EditTodoForm editTodo={editTask} task={todo} />
                )
            ))}
            <Stateless score="33" />
        </div>
    )
}