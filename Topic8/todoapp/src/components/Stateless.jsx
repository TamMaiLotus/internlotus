import React from 'react'

class Stateless extends React.PureComponent {
    render() {
        const { score } = this.props;
        console.log(score);
        return (
            <div>
                <span>This is a stateless component: {score}%</span>
            </div>
        )
    }
}

export default Stateless;