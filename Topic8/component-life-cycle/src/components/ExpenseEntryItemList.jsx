import React from 'react';
import './ExpenseEntryItemList.css'
class ExpenseEntryItemList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: this.props.items
        }

        this.handleMouseEnter = this.handleMouseEnter.bind();
        this.handleMouseLeave = this.handleMouseLeave.bind();
    }
    handleMouseEnter(e) {
        e.target.parentNode.classList.add("highlight");
    }
    handleMouseLeave(e) {
        e.target.parentNode.classList.remove("highlight");
    }
    handleDelete = (id, e) => {
        e.preventDefault();
        console.log(id);

        this.setState((state, props) => {
            let items = [];

            state.items.forEach((item, id) => {
                if (item.id != id)
                    items.push(item)
            })

            let newState = {
                items: items
            }
            return newState;
        })
    }

    getTotal() {
        let total = 0;
        for (var i = 0; i < this.state.items.length; i++) {
            total += this.state.items[i].amount
        }
        return total;
    }

    componentDidMount() {
        console.log("ExpenseEntryItemList :: Initialize :: componentDidMount :: Component mounted");
       }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("ExpenseEntryItemList :: Update :: shouldComponentUpdate invoked :: Before update");
        return true;
    }
    static getDerivedStateFromProps(props, state) {
        console.log("ExpenseEntryItemList :: Initialize / Update :: getDerivedStateFromProps :: Before update");
        return null;
    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log("ExpenseEntryItemList :: Update :: getSnapshotBeforeUpdate :: Before update");
        return null;
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("ExpenseEntryItemList :: Update :: componentDidUpdate :: Component updated");
       }
    componentWillUnmount() {
        console.log("ExpenseEntryItemList :: Remove :: componentWillUnmount :: Component unmounted");
       }

    render() {
        const lists = this.state.items.map((item) =>
            <tr key={item.id} onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}>
                <td>{item.name}</td>
                <td>{item.amount}</td>
                <td>{new Date(item.spendDate).toDateString()}</td>
                <td>{item.category}</td>
                <td><a href="#"
                    onClick={(e) => this.handleDelete(item.id, e)}>Remove</a></td>
            </tr>
        );
        return (
            <table>
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                    {lists}
                    <tr>
                        <td colSpan="1" style={{ textAlign: "right" }}>Total
                            Amount</td>
                        <td colSpan="4" style={{ textAlign: "left" }}>
                            {this.getTotal()}
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}
export default ExpenseEntryItemList;