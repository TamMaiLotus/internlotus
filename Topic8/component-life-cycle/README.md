# Component Life Cycle
## Life Cycle API 
Mỗi React component có 3 stage khác nhau: 
![life-cycle](./public/life-cycle.png)
* **Mounting:** Tượng trưng cho việc render của 1 phần tử React trong DOM node được cho, gồm 4 phương thức:
  * ```constructor()```: được gọi trong giai đoạn khởi tạo một React component. Dùng để set state khởi đầu và thuộc tính của component
  * ```getDerivedStateFromProps()```: được gọi trong giai đoạn khởi tạo và update và trước giai đoạn render. Nó trả về một object state mới.
   Chủ yếu dùng trong các animation.
  * ```render()```: được gọi sau khi việc xây dựng component được hoàn thành. Nó render component trong DOM ảo, hay nói cách khác là mount component trong cây DOM.
  * ```componentDidMount()```: được gọi sau quá trình mount component vào DOM tree. Là nơi để chạy các statement mà cần việc component đã được đặt vào DOM.
* **Updating:** Tượng trưng cho việc re-render của 1 phần tử React trong DOM node được trong lúc state thay đổi / update.
  * ```shouldComponentUpdate()```: được khởi tạo trong giai đoạn Update. Dùng để quyết định liệu component nên update hay ko. Nếu false, thì việc update sẽ không xảy ra
  ```
  shouldComponentUpdate(nextProps, nextState)
  ```
  nextProps, nextState: prop và state tiếp theo cho component.
  * ```getDerivedStateFromProps()```
  * ```render()```
  * ```getSnapshotBeforeUpdate()```: được khởi tạo ngay trước khi nội dung được render và đưa vào cây DOM. Chủ yếu được sử dụng để lấy giá trị trước khi thông tin được update.
  * ```componentDidUpdate()```: được gọi sau khi component được cập nhật trên DOM. Dùng để lấy thông tin của component sau update.
* **Unmounting:** Tượng trưng cho việc bỏ một React component, gồm:
  * ```componentWillUnmount()``` được khởi tạo sau khi component được unmount khỏi cây DOM. Là lúc để clean up object