# State

## Khái niệm:
- State là một object có thể được sử dụng để chứa dữ liệu hoặc thông tin về components, tức trạng thái của một component. State có thể được thay đổi bất cứ khi nào mong muốn. 
- State chỉ tồn tại trong phạm vi components chứa nó.
- Khi state của 1 component thay đổi, component sẽ gọi hàm render() đến để re-render lại.

## Stateful và Stateless (React 18): 
**Stateless** Stateless component 
* là các component không chứa state
* là các component chỉ nhận props từ thằng cha và trả về các JSX elements.
* chỉ dùng để show gì đó trên màn hình

**Stateful** Stateful Component
* là các component chứa state
* là các component nhận cả props và state, đóng vai trò với mọi life cycle trong React. Component này sẽ thay đổi State.
* dùng để thêm chức năng cho component

## State và Props: 
đều truyền và xử lí data giữa các component.
**Props:**: Truyền data từ thằng cha xuống, data này xuống con sẽ chỉ là read-only, ko thể thay đổi bởi thằng con. Cách duy nhất để thay đổi giá trị là gửi signal đến thằng cha và thay đổi nó.
**State** : 
* Data ở đây bị control hoàn toàn bởi component
* Có thể truyền các loại value như number, string, boolean, ...
* Chỉ có chính nó mới có thể thay đổi state bên trong