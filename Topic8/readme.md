## Summary week 1:

**ReactJS:**

1. K/n: React Js là một thư viện viết bằng javascript, dùng để xây dựng giao diện người dùng (UI). React được sử dụng rộng rãi và có hệ sinh thái đa dạng phong phú.
2. Chức năng:
- Kiến trúc cơ sở vững chắc
- Tính mở rộng kiến trúc
- Các thư viện dựa trên thành phần
- Kiến trúc thiết kế JSX
- Tính khai báo của thư viện UI

(Khác biệt giữa lib và framework - Angular vs React)

**HTML:** Là ngôn ngữ giúp tạo các trang web đơn lẻ, giúp thiết kế, trình bày các thành phần trên trang web rõ ràng, súc tích
**CSS:** Là ngôn ngữ để định dạng các thành phần của các thẻ trong HTML

(Check thêm CSS, SASS, SCSS và practice)

**SASS:** Là ngôn ngữ tiền xử lí của CSS, giúp cho đoạn code có các logic hoạt động như một ngôn ngữ code bình thường
**ES6:** Là bản mới nhất chuẩn ECMAScript, là tập hợp các kĩ thuật nâng cao của JS (let và const, arrow functions, destructring, classes, ...)

**Bootstrap:** Là một framework tập hợp các cú pháp và component giúp build website