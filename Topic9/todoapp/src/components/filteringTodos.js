export function filterTodos(todos, tab) {
    return todos.filter(todo => {
        switch (tab) {
            case 'all': return true;
            case 'active': return !todo.completed;
            case 'completed': return todo.completed;
        }
    })
}