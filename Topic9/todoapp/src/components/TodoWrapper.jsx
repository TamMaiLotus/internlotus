import React, {useState, useEffect, useMemo} from 'react';
import {v4 as uuidv4} from 'uuid';
uuidv4();

import { TodoForm } from './TodoForm';
import { Todo } from './Todo';
import { EditTodoForm } from './EditTodoForm';
import { filterTodos } from './filteringTodos';

export const TodoWrapper = () => {
    const [todos, setTodos] = useState([])
    const [todoRemainings, setTodoRemainings] = useState(0);
    const [tab, setTab] = useState('all');

    const visibleTodo = useMemo(() => filterTodos(todos, tab), [todos, tab]);

    useEffect(() => {
        setTodoRemainings(todos.filter(todo => !todo.completed).length)
    }, [todos]);

    const addTodo = todo => {
        setTodos([
            ...todos, 
            {id: uuidv4(), task: todo, completed: false, isEditing: false
        }])
        console.log(todos)
    }

    const toggleComplete = id => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                completed: !todo.completed
            } : 
                todo
            ))
    }

    const editTodo = id => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                isEditing: !todo.isEditing 
            } : 
                todo
            ))
    }

    const editTask = (task, id) => {
        setTodos(todos.map(todo => todo.id === id
            ? {
                ...todo,
                task,
                isEditing: !todo.isEditing
            } : 
                todo
            ))
    }
    const deleteTodo = id => {
        setTodos(todos.filter(todo => todo.id !== id))
    }
    return (
        <div className='TodoWrapper'>
            <h1>Task remains: {todoRemainings}</h1>
            <button onClick={() => setTab('all')}>All</button>
            <button onClick={() => setTab('active')}>Active</button>
            <button onClick={() => setTab('completed')}>Completed</button>
            <TodoForm addTodo={addTodo}/>
            {visibleTodo.map((todo, index) => (
                !todo.isEditing ? (
                    <Todo task={todo} key={index}
                    toggleComplete={toggleComplete}
                    deleteTodo={deleteTodo}
                    editTodo={editTodo} />
                ) : (
                    <EditTodoForm editTodo={editTask} task={todo} />
                )
            ))}
            
        </div>
    )
}