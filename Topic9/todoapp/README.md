# React Hooks

## useState:

* Giúp quản lí các state bên trong một functional component
* Trả về một mảng 2 phần tử ```[name, setName]```
  * name: Giá trị hiện tại của state
  * setName: Update giá trị của state và trigger re-render

## useEffect:

* Quản lí vòng đời của một component, thay thế cho 3 method Lifecycle trong class component

* Các method LifeCycle:
  * ## ```componentDidMount```

```
import React from "react";
 
class Component extends React.Component {
  componentDidMount() {
    console.log("Behavior before the component is added to the DOM");
  }
 
  render() {
    return <h1>Hello World</h1>;
  }
};
```
với Hooks: 
```
import React, { useEffect } from "react";
 
const Component = () => {
  useEffect(() => {
    console.log("Behavior before the component is added to the DOM");
  }, []);
 
  return <h1>Hello World</h1>;
};
```
Để ý mảng trống ```[]``` được đặt ở đối số thứ 2, điều này để nói với hook ```useEffect``` rằng chỉ cần chạy 1 lần khi component mount.

  * ## ```componentDidUpdate```
```
import React from "react";
 
class Component extends React.Component {
  componentDidUpdate() {
    console.log("Behavior when the component receives new state or props.");
  }
 
  render() {
    return <h1>Hello World</h1>;
  }
};
```
với Hooks:
```
import React, { useEffect } from "react";
 
const Component = () => {
  useEffect(() => {
    console.log("Behavior when the component receives new state or props.");
  });
 
  return <h1>Hello World</h1>;
};
```
Ở đây, đối số thứ 2 ```[]``` ko còn, tức là sẽ không có dependacy để mà đánh giá khi nào useEffect nên trigger. Bởi vậy, useEffect sẽ chạy mỗi lần re-render.

  (Khi truyền 1 đối số vào ```[]```, useEffect sẽ chỉ chạy khi component được mount hoặc giá trị đối số đó thay đổi)

  * ## ```componentWillUnmount```
```
import React from "react";
 
class Component extends React.Component {
  componentWillUnmount() {
    console.log("Behavior right before the component is removed from the DOM.");
  }
 
  render() {
    return <h1>Hello World</h1>;
  }
};
```
với Hooks:
```
import React, { useEffect } from "react";
 
const Component = () => {
  useEffect(() => {
    return () => {
            console.log("Behavior right before the component is removed from the DOM.");
        }
  }, []);
 
  return <h1>Hello World</h1>;
};
```
```return``` ở trong useEffect dùng để gọi chỉ khi nào component được bỏ khỏi DOM. Thường dùng để cleanup component để chuẩn bị cho cycle mới

## useMemo: 
* Lưu lại kết quả của hàm nào và những giá trị nào sẽ làm thay đổi kết quả đó giữa các lần re-render, tránh lặp đi lặp lại các logic tính toán nặng nề
```
const visibleTodo = useMemo(() => filterTodos(todos, tab), [todos, tab]);
 ```
* Nếu dependencies trong ```[]``` thay đổi, thì hàm tính toán trong useMemo sẽ được thực thi lại, từ đó trả ra giá trị mới. Nếu không, useMemo sẽ trả lại kết quả trước đó mà không phải thông qua hàm tính toán, từ đó tăng performance, tránh phải recalculate và re-render.

## useCallback:
* Giải quyết tình trạng component con luôn bị re-render khi các callback function ở functional component cha pass xuống.
```
const handleSubmit = useCallback((e) => {
      //your code here
      }, [value]);
 ```
* Nếu dependencies trong ```[]``` thay đổi, useCallback sẽ trả về một function bạn pass vào, callback function này sẽ được tái tạo. Còn nếu không thay đổi, thì function pass xuống component con không bị tạo mới, từ đó component con sẽ không bị re-render

Khi nào dùng Hooks, Hooks life-cycle, array function trong JS.