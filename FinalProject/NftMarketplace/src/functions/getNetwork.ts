import useActiveWeb3React from "../hooks/useActiveWeb3React";

export function getNetwork() {
    const {chainId, provider} = useActiveWeb3React();
    return {
        chainId,
        provider
    }
}