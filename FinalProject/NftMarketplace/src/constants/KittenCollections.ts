export const KittenCollections = [
    {   
        id: 1, 
        src: '/src/assets/images/kitten_1.png',
        price: 0.044,
        apr: 0.4743,
    },
    { 
        id: 2,
        src: '/src/assets/images/kitten_2.png',
        price: 0.045,
        apr: 0.4742,
    },
    {   id: 3,
        src: '/src/assets/images/kitten_3.png',
        price: 0.046,
        apr: 0.4741,
    },
    {   
        id: 4,
        src: '/src/assets/images/kitten_4.png',
        price: 0.047,
        apr: 0.4740,
    },
    { 
        id: 5,
        src: '/src/assets/images/kitten_5.png',
        price: 0.048,
        apr: 0.4739,
    },
]