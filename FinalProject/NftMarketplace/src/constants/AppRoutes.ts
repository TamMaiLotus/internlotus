export const AppRouters = {
    HOME: '/',
    BUY: '/buy',
    BUY_COLLECTION: '/collections',
    BUY_ITEM: '/items',
    SELL: '/sell',
    ACCOUNT: '/account/:accountId',
    APPROVAL: '/approval/:accountId',
  };