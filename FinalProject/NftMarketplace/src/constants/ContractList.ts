import ERC721_ABI from './ERC721_ABI.json'
import ERC20_ABI from './ERC20_ABI.json'
import VAULT_ABI from './VAULT_ABI.json'

export const VaultContract = {
    address: '0xB37f99CAD2B7Dc870A2E4e385cbA1AD2E759Fd50' as `0x${string}`,
    abi: VAULT_ABI,
}
export const ERC20Contract = {
    address: '0xACcBB14e785d63Da658CB60e78e94545dFE04dF9' as `0x${string}`,
    abi: ERC20_ABI,
}
export const ERC721Contract = {
    address: '0xC0edBaC4Eb7A2B586734324112c32668f7019F34' as `0x${string}`,
    abi: ERC721_ABI,
} 