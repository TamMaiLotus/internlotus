import { useNFTContract } from './useNFTContract'
import { useEffect, useState } from 'react'
import { Provider } from '@ethersproject/providers'
import { useNetwork, useProvider } from 'wagmi'

function useGetApproved(tokenId: number) {
  const {chain} = useNetwork();
  const provider = useProvider();
  const [result, setResult] = useState(null);

  const { getApproved } = useNFTContract(
    chain.id,
    provider as Provider,
  )

  useEffect(() => {
    const fetchOwner = async () => {
      const ownerAddr = await getApproved(tokenId)
      setResult(ownerAddr)
    }

    fetchOwner()
  }, [tokenId])


  return {
    result
  }
}

export default useGetApproved