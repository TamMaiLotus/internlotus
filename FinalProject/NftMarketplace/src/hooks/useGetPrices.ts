import { useVaultContract } from './useVaultContract'
import { useEffect, useState } from 'react'
import { Provider } from '@ethersproject/providers'
import { useNetwork, useProvider } from 'wagmi'
import { formatEther } from 'ethers/lib/utils.js';

function useGetPrices(tokens: any[]) {
  const {chain} = useNetwork();
  const provider = useProvider();
  const [result, setResult] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const { getPrice } = useVaultContract(
    chain.id,
    provider as Provider,
  )

  useEffect(() => {
    const fetchPrice = async () => {
      setIsLoading(true);
      try {
      const tokensWithPrice = await Promise.all(tokens.map(async (token) => {
        const price = await getPrice(token.id);
        const formattedPrice = formatEther(price);
        const tokenWithPrice = {
          ...token,
          formattedPrice,
        };
        localStorage.setItem(`key${token.id}`,JSON.stringify(tokenWithPrice));
        return {
          tokenWithPrice
        };
      }));
      setResult(tokensWithPrice)
    } catch (e: any) {
      setError(e.message);
    } finally {
      setIsLoading(false);
    }}

    fetchPrice()
  }, [tokens])

  return {
    result,
    isLoading,
    error
  }
}

export default useGetPrices
