/* eslint-disable react-hooks/exhaustive-deps */
import { BigNumber, providers, Signer } from "ethers";
import { useAccount, useContract } from "wagmi";
import { useCallback } from "react";
import { ERC20Contract } from "../constants/ContractList";
import { parseEther } from "ethers/lib/utils.js";

export const useBCOContract = (
  chainId: number | undefined,
  signerOrProvider: Signer | providers.Provider
) => {
  const { address: account } = useAccount();
  const contractInstance = useContract({
    ...ERC20Contract,
    signerOrProvider,
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const executeLogic = async (callback: () => any) => {
    try {
      if (!contractInstance) return;
      if (contractInstance && account) {
        return callback();
      }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      console.error(e);
    }
  };

  //Read function

  const name = useCallback(async (): Promise<string> => {
    return await executeLogic(() => {
      return contractInstance?.name();
    });
  }, [account, chainId, signerOrProvider]);

  const symbol = useCallback(async (): Promise<string> => {
    return await executeLogic(() => {
      return contractInstance?.symbol();
    });
  }, [account, chainId, signerOrProvider]);

  const allowance = useCallback(
    async (owner: string): Promise<BigNumber> => {
      return await executeLogic(() => {
        const param = [owner, '0xB37f99CAD2B7Dc870A2E4e385cbA1AD2E759Fd50'];
        return contractInstance?.allowance(...param);
      });
    },
    [account, chainId, signerOrProvider]
  )

  // Write function
  const approve = useCallback(
    async (spender: string) => {
      return await executeLogic(async () => {
        const param = [spender, parseEther('10')];
        const gasLimit = await contractInstance?.estimateGas.approve(...param);
        const tx = await contractInstance?.approve(...param, {
          gasLimit: BigNumber.from(gasLimit).mul(2),
        });
        return tx.wait(1);
      });
    },
    [account, chainId, signerOrProvider]
  );
  return {
    // Read function
    name,
    symbol,
    allowance,
    // Write function
    approve,
  };
};