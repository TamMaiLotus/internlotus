/* eslint-disable react-hooks/exhaustive-deps */
import { BigNumber, providers, Signer } from "ethers";
import { useAccount, useContract } from "wagmi";
import { useCallback } from "react";
import { ERC721Contract } from "../constants/ContractList";

export const useNFTContract = (
  chainId: number | undefined,
  signerOrProvider: Signer | providers.Provider
) => {
  const { address: account } = useAccount();
  const contractInstance = useContract({
    ...ERC721Contract,
    signerOrProvider,
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const executeLogic = async (callback: () => any) => {
    try {
      if (!contractInstance) return;
      if (contractInstance && account) {
        return callback();
      }
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      console.error(e);
    }
  };

  //Read function
  const owner = useCallback(async (): Promise<string> => {
    return await executeLogic(() => {
      return contractInstance?.owner();
    });
  }, [account, chainId, signerOrProvider]);

  const name = useCallback(async (): Promise<string> => {
    return await executeLogic(() => {
      return contractInstance?.name();
    });
  }, [account, chainId, signerOrProvider]);

  const balanceOf = useCallback(
    async (owner: string): Promise<string> => {
      return await executeLogic(() => {
        const param = [owner];
        return contractInstance?.balanceOf(...param).then((balance: number) => balance.toString());
      });
    },
    [account, chainId, signerOrProvider]
  )

  const ownerOf = useCallback(
    async (tokenId: number): Promise<string> => {
      return await executeLogic(() => {
        const param = [tokenId];
        return contractInstance?.ownerOf(...param);
      });
    },
    [account, chainId, signerOrProvider]
  )

  const getApproved = useCallback(
    async (tokenId: number): Promise<string> => {
      return await executeLogic(() => {
        const param = [tokenId];
        return contractInstance?.getApproved(...param);
      });
    },
    [account, chainId, signerOrProvider]
  )


  // Write function
  const mint = useCallback(
    async (to: string, tokenId: number) => {
      return await executeLogic(async () => {
        const param = [to, tokenId];
        const gasLimit = await contractInstance?.estimateGas.mint(...param);
        const tx = await contractInstance?.mint(...param, {
          gasLimit: BigNumber.from(gasLimit).mul(2),
        });
        return tx.wait(1);
      });
    },
    [account, chainId, signerOrProvider]
  );

  const approve = useCallback(
    async (to: string, tokenId: number) => {
      return await executeLogic(async () => {
        const param = [to, tokenId];
        const gasLimit = await contractInstance?.estimateGas.approve(...param);
        const tx = await contractInstance?.approve(...param, {
          gasLimit: BigNumber.from(gasLimit).mul(2),
        });
        return tx.wait(1);
      });
    },
    [account, chainId, signerOrProvider]
  );

  return {
    // Read function
    owner,
    balanceOf,
    ownerOf,
    getApproved,
    name,
    // Write function
    mint,
    approve,
  };
};