
import { useEffect, useState } from 'react'
import { Provider } from '@ethersproject/providers'
import { useNetwork, useProvider } from 'wagmi'
import { useBCOContract } from './useBCOContract';

export function useReadBCO() {
    const { chain } = useNetwork();
    const provider = useProvider();
    const [result, setResult] = useState('');

    const { symbol } = useBCOContract(
        chain.id,
        provider as Provider,
    )

    useEffect(() => {
        symbol().then(symbolString => {
            setResult(symbolString);
        });
    }, []); 

    return result;
}
