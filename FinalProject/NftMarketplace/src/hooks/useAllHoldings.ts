import { useVaultContract } from './useVaultContract'
import { useEffect, useState } from 'react'
import { JsonRpcSigner, Provider } from '@ethersproject/providers'
import { useNetwork, useProvider, useSigner } from 'wagmi'

function useAllHoldings() {
  const { chain } = useNetwork();
  const provider = useProvider();
  const {data: signer} = useSigner();
  const [result, setResult] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const { allHoldings } = useVaultContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  useEffect(() => {
    const fetchHoldings = async () => {
      setIsLoading(true);
      try {
        const listIds = await allHoldings();
        let parsed = listIds.reduce((acc, item) => {
          const parsedItem = JSON.parse(localStorage.getItem(`key${item}`));
          if (parsedItem) {
            acc.push(parsedItem);
          }
          return acc;
        }, []);
        setResult(parsed); 
      } catch (e: any) {
        setError(e.message);
      } finally {
        setIsLoading(false);
      }
    }
    fetchHoldings()
  }, [allHoldings])

  return {
    result,
    isLoading,
    error,
  }
}

export default useAllHoldings