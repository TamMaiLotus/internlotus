import { useVaultContract } from './useVaultContract'
import { useCallback, useEffect, useState } from 'react'
import { Provider } from '@ethersproject/providers'
import { useNetwork, useProvider } from 'wagmi'
import { formatEther } from 'ethers/lib/utils.js';

function useGetPrice(tokenId: number) {
  const {chain} = useNetwork();
  const provider = useProvider();
  const [result, setResult] = useState('');

  const { getPrice } = useVaultContract(
    chain.id,
    provider as Provider,
  )

  const getPriceCallback = useCallback(getPrice, []);

  useEffect(() => {
    const fetchPrice = async () => {
      const price = await getPrice(tokenId);
      const formattedPrice = formatEther(price);
      setResult(formattedPrice)
    }

    fetchPrice()
  }, [getPriceCallback, tokenId])

  return {
    result,
  }
}

export default useGetPrice
