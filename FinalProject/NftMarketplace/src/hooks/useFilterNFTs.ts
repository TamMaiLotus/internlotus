import { useState, useEffect, useCallback } from 'react';
import { getNFTIds } from "../functions/getNFTIds"

export const useFilterNFTs = (accountId: string) => {
    const [nfts, setNfts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    const filterNFTs = useCallback(async () => {
        setIsLoading(true);
        try {
        const {nftList} = await getNFTIds(accountId);
        let listNfts = [];
        for (let nft of nftList) {
            if (nft.contract.name==="NFT721")
            {
                const tokenId = nft.tokenId;
                const storedValue = JSON.parse(localStorage.getItem(`key${tokenId}`));
                listNfts.push(storedValue)
            }
        }
        setNfts(listNfts);
    } catch (e: any) {
        setError(e.message);
    } finally {
        setIsLoading(false)
    }}, [accountId]);

    useEffect(() => {
        filterNFTs();
    }, [filterNFTs]);

    return {
        nfts,
        isLoading,
        error
    }
}
