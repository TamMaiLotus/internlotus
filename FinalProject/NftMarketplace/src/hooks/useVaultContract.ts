/* eslint-disable react-hooks/exhaustive-deps */
import { BigNumber, providers, Signer } from "ethers";
import { useAccount, useContract } from "wagmi";
import { useCallback } from "react";
import { VaultContract } from "../constants/ContractList";

export const useVaultContract = (
    chainId: number | undefined,
    signerOrProvider: Signer | providers.Provider
) => {
    const { address: account } = useAccount();
    const contractInstance = useContract({
        ...VaultContract,
        signerOrProvider,
    });

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const executeLogic = async (callback: () => any) => {
        try {
            if (!contractInstance) return;
            if (contractInstance && account) {
                return callback();
            }
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            console.error(e);
        }
    };

    //Read function
    const allHoldings = useCallback(async (): Promise<number[]> => {
        return await executeLogic(() => {
            return contractInstance?.allHoldings();
        });
    }, [account, chainId, signerOrProvider]);

    const totalHoldings = useCallback(async (): Promise<number> => {
        return await executeLogic(() => {
            return contractInstance?.totalHoldings();
        });
    }, [account, chainId, signerOrProvider]);

    const getOwner = useCallback(
        async (tokenId: number): Promise<string> => {
            return await executeLogic(() => {
                const param = [tokenId];
                return contractInstance?.getOwner(...param);
            });
        },
        [account, chainId, signerOrProvider]
    )

    const getPrice = useCallback(
        async (tokenId: number): Promise<BigNumber> => {
            return await executeLogic(() => {
                const param = [tokenId];
                return contractInstance?.getPrice(...param);
            });
        },
        [account, chainId, signerOrProvider]
    )


    // Write function
    const buyToken = useCallback(
        async (tokenId: number) => {
            return await executeLogic(async () => {
                const gasLimit = await contractInstance?.estimateGas.buyToken(tokenId);
                const tx = await contractInstance?.buyToken(tokenId, {
                    gasLimit: BigNumber.from(gasLimit).mul(2),
                });
                return tx.wait(1);
            });
        },
        [account, chainId, signerOrProvider]
    );

    const sellToken = useCallback(
        async (tokenId: number) => {
            return await executeLogic(async () => {
                const gasLimit = await contractInstance?.estimateGas.sellToken(tokenId);
                const tx = await contractInstance?.sellToken(tokenId, {
                    gasLimit: BigNumber.from(gasLimit).mul(2),
                });
                return tx.wait(1);
            });
        },
        [account, chainId, signerOrProvider]
    );

    const setPrice = useCallback(
        async (tokenId: number, price: BigInt) => {
          return await executeLogic(async () => {
            const gasLimit = await contractInstance?.estimateGas.setPrice([tokenId], [price]);
            const tx = await contractInstance?.setPrice([tokenId], [price], {
              gasLimit: BigNumber.from(gasLimit).mul(2),
            });
            return tx.wait(1);
          });
        },
        [account, chainId, signerOrProvider]
      );


    return {
        // Read function
        allHoldings,
        totalHoldings,
        getPrice,
        getOwner,
        // Write function
        buyToken,
        sellToken,
        setPrice,
    };
};