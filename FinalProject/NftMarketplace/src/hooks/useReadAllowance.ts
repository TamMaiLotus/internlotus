import { useBCOContract } from './useBCOContract'
import { useEffect, useState } from 'react'
import { Provider } from '@ethersproject/providers'
import { useAccount, useNetwork, useProvider } from 'wagmi'
import { BigNumber } from 'ethers';

function useReadAllowance() {
    const { chain } = useNetwork();
    const provider = useProvider();
    const [result, setResult] = useState<BigNumber|null>(null);
    const { address } = useAccount();

    const { allowance } = useBCOContract(
        chain.id,
        provider as Provider,
    )

    useEffect(() => {
        allowance(address).then(allowance => {
            setResult(allowance);
        });
      }, [address]);
      
    return {
        result,
    }
}

export default useReadAllowance
