import styled from "styled-components";
import { ColumnFlexContainer } from "./FlexContainer";

export const TitleContainer = styled(ColumnFlexContainer)`
    position: relative;
`

export const TitleNameContainer = styled(TitleContainer)`
    z-index: 2;
    float: left;
`

export const NumberOfItemContainer = styled(TitleContainer)`
    z-index: 1;
    float: right;
    text-align: right;
`

export const TagNumber = styled.span`
    position: absolute;
    color: white;
    top: 15px;
    left: 15px;
    font-size: 20px;
`