import styled from "styled-components";

export const RowFlexContainer = styled.div`
    display: flex;
    flex-direction: row;
    color: var(--primary-color);
`

export const ColumnFlexContainer = styled.div`
    display: flex;
    flex-direction: column;
    color: var(--primary-color);
`
