import styled from "styled-components";

export const GridView = styled.div`
    display: grid;
    gap: 1rem;
    margin-top: 20px;
    padding: 40px;
    background-color: var(--background-blue-color);
`
