import styled from "styled-components";

export const ConfirmButton = styled.button`
    background-color: var(--primary-bold-color);
    border-radius: 15px;
    color: white;
    padding-top: 5px;
    padding-bottom: 5px;
`

export const CancelButton = styled.button`
    background-color: transparent;
    color: var(--grey-color);
    padding-top: 5px;
    padding-bottom: 5px;
`