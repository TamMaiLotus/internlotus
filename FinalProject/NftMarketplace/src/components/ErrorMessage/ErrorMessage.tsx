import React from "react";

interface ErrorProps {
    place: string;
    message: string;
}

export const ErrorMessage: React.FC<ErrorProps> = ({place, message}) => {
    return (
        <div style={{color: 'red'}}>Error in {place}: {message}</div>
    )
}