import styled from "styled-components";
import Button from 'react-bootstrap/Button';

export const ConnectButton = styled(Button)`
    background-color: var(--primary-color);
    border-radius: 15px;
    font-size: 20px;
    width: 250px;
    height: 60px;
`

export const StyledDashboard = styled.div`
    display: flex;
    flex-direction: column;
    width: 90vw;
`

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    gap: 15px;
    text-align: center;
    align-items: center;

    color: var(--primary-color);
    height: 75vh;
    margin-top: 1rem;
    justify-content: center;
`