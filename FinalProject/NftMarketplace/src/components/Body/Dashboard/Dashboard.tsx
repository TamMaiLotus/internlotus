import { AppRouters } from "@/constants/AppRoutes";
import { useNavigate } from "react-router-dom";
import { useAccount, useDisconnect } from "wagmi"
import { ConnectButton, StyledDashboard, Content } from "./styled";
import { useState } from "react";
import { ConnectWallet } from "@/components/popups/createWalletModal/ConnectWalletModal";

export const Dashboard = () => {
    const navigate = useNavigate();
    const { address, isConnected } = useAccount({
        onConnect() {
            navigate(AppRouters.BUY);
        },
        onDisconnect() {
            navigate(AppRouters.HOME);
        }
    });
    const { disconnect } = useDisconnect();
    const [openModal, setOpenModal] = useState('')
    return (
        <StyledDashboard>
            <h2 style={{ marginTop: '20px' }}>NFT Marketplace</h2>
            <Content>
                {!(isConnected) ? (
                    <>
                        <h1>Uh oh...</h1>
                        <h3>It seem like you haven't connected to your wallet yet.</h3>
                        <ConnectButton onClick={() => setOpenModal('Connect')}>Connect now</ConnectButton>
                        {openModal === 'Connect' && <ConnectWallet setOpenModal={setOpenModal} />}
                    </>
                ) : (
                    <>
                        <h1>You are connected!</h1>
                        <h3>Address: {address}</h3>
                        <ConnectButton onClick={disconnect}>Disconnect your wallet</ConnectButton>
                    </>
                )}
            </Content>
        </StyledDashboard>
    )
}