import { useParams } from "react-router-dom";
import { AccountTitleItem } from "./AccountTitleItem/AccountTitleItem";
import { StyledSingleCollectionPage } from "./styled";
import { useState } from "react";
import { StyledCollectionView } from "./CollectionView/styled";
import { NftItemsLayout } from "./CollectionView/NftItemsLayout/NftItemsLayout";
import { StyledNftItemsLayout } from "./CollectionView/NftItemsLayout/styled";
import { useFilterNFTs } from "@/hooks/useFilterNFTs";
import { SidebarLayout } from "./CollectionView/SidebarLayout/SidebarLayout";
import { Loader } from "@/components/Loader/Loader";
import { ErrorMessage } from "@/components/ErrorMessage/ErrorMessage";

export const AccountPage = () => {
    const { accountId } = useParams();
    const {nfts, isLoading, error} = useFilterNFTs(accountId);
    const [selectedItem, setSelectedItem] = useState(null);

    if (isLoading) 
        return <Loader/>

    if (error)
        return <ErrorMessage place="useFilterNFTs" message={error}/>
    return (
        <StyledSingleCollectionPage>
            <AccountTitleItem />
            <StyledCollectionView>
                <StyledNftItemsLayout>
                    {(nfts) ? (nfts.filter(item => item && item.id !== null) .map((item) => (
                        <NftItemsLayout
                            key={item.id}
                            collectionItem={item}
                            isChosen={item.id === selectedItem}
                            onSelect={() => setSelectedItem(item.id === selectedItem ? null : item.id)} />
                    ))
                    ) : (
                        <></>
                    )}
                </StyledNftItemsLayout>
                <SidebarLayout selectedId={selectedItem}/>
            </StyledCollectionView>
        </StyledSingleCollectionPage>
    );
}