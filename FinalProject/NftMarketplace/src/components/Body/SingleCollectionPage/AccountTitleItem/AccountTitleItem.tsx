import { AccountNameContainer, CircleIconContainer, StyledAccountTitleItem } from "./Style";
import { useParams } from "react-router-dom";

interface AccountTitleItemProps { }

export const AccountTitleItem: React.FunctionComponent<AccountTitleItemProps> = (props) => {

  const { accountId } = useParams();
  const shortenedAccountId = accountId?.substring(0,4) + '...' + accountId?.slice(-4);
  return (
    <StyledAccountTitleItem>
      <AccountNameContainer>
        <CircleIconContainer />
        <h1>{shortenedAccountId}</h1>
      </AccountNameContainer>
    </StyledAccountTitleItem>
  );
};