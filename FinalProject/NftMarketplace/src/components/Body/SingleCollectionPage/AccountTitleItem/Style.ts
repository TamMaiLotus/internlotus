import styled from "styled-components";
import { RowFlexContainer } from "../../../../components/GlobalStyles/FlexContainer";

export const StyledAccountTitleItem = styled(RowFlexContainer)`
    column-gap: 40px;
    align-items: center;
`

export const CircleIconContainer = styled.div`
    width: 64px;
    height: 64px;
    border-radius: 56px;
    background-color: var(--background-blue-color);
`

export const AccountNameContainer = styled(StyledAccountTitleItem)`
    column-gap: 20px;
`