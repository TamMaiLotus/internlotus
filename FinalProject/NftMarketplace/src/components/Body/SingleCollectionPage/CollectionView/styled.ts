import { RowFlexContainer } from "../../../GlobalStyles/FlexContainer";
import styled from "styled-components";

export const StyledCollectionView = styled(RowFlexContainer)`
    height: 100%;
    margin-top: 30px;
    background-color: var(--background-blue-color);
`;