import { StyledSidebarLayout } from "./styled";
import useGetApproved from "@/hooks/useGetApproved";
import { useEffect, useState } from "react";
import ApproveToken from "@/components/NFTToken/ApproveToken";
import { Form } from "react-bootstrap";
import useGetPrice from "@/hooks/useGetPrice";
import { ButtonContainer } from "@/components/popups/createNftModal/styled";
import PriceSell from "@/components/NFTToken/PriceSell";
import SellToken from "@/components/NFTToken/SellToken";

interface SidebarLayoutProps {
  selectedId: number,
}

const InvalidAddress = '0x0000000000000000000000000000000000000000'

export const SidebarLayout: React.FunctionComponent<SidebarLayoutProps> = ({ selectedId }) => {
  const [stateNft, setStateNft] = useState('');
  const [price, setPrice] = useState('');
  const [priceState, setPriceState] = useState(false);
  const [sellState, setSellState] = useState(false);
  const { result: approvedAddress } = useGetApproved(selectedId);
  const { result: orgPrice } = useGetPrice(selectedId);

  useEffect(() => {
    if (selectedId === null) {
      setStateNft('Not Selected');
    } else if (approvedAddress !== InvalidAddress) {
      setStateNft('Approved');
    } else {
      setStateNft('Not Approved');
    }
  }, [selectedId, approvedAddress]);

  useEffect(() => {
    console.log(price);
    const checkPrice = ((price === orgPrice) || (price === '0') || (price === ''));
    setPriceState(checkPrice);
  }, [price, orgPrice]);

  const handleStatePrice = () => {
    // Call your set price function here
    setPrice('');
    setPriceState(false);
    setSellState(true);
  };

  const handleStateSell = () => {
    setSellState(false);
  }

  const handleValueChange = (e: any) => {
    setPrice(e.target.value);
    if (e.target.value === '') {
      setSellState(true);
      setPriceState(false);
    } else {
      setSellState(false);
      setPriceState(true);
    }
  }

  return (
    <StyledSidebarLayout>
      {(stateNft === 'Approved') ? (
        <div>
          <Form>
            <Form.Group controlId="formFile" className="mb-3">
              <Form.Label>Set Price</Form.Label>
              <Form.Control
                type="number"
                className="mb-4"
                step="0.1"
                size="lg"
                placeholder={orgPrice}
                value={price}
                onChange={e => handleValueChange(e)} />
              <ButtonContainer>
                <PriceSell id={selectedId} price={price} state={priceState} onClick={handleStatePrice} />
                <SellToken id={selectedId} state={sellState} onClick={handleStateSell} />
              </ButtonContainer>
            </Form.Group>

          </Form>
        </div>
      ) : (stateNft === 'Not Approved') ? (
        <>
          <span>It seem like you haven't approved this token yet</span>
          <ApproveToken id={selectedId}>Approve</ApproveToken>
        </>
      ) : (<div>Select one token to view the status of it.</div>)}
    </StyledSidebarLayout>
  );
};