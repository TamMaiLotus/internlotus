import styled from "styled-components";
import { ColumnFlexContainer } from "../../../../../components/GlobalStyles/FlexContainer";

export const StyledSidebarLayout = styled(ColumnFlexContainer)`
    justify-content: center;
    align-items: stretch;
    align-content: center;
    text-align: center;
    row-gap: 15px;
    flex: 1 0 200px;
    background-color: var(--light-blue-color);
    padding: 30px;
`;

