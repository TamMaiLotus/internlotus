import { GridView } from "../../../../GlobalStyles/GridContainer";
import styled, {css} from "styled-components";

interface CheckProps {
    isChosen: boolean;
}

export const StyledNftItemsLayout = styled(GridView)`
    grid-template-columns: repeat(3,350px);
`;

export const NftImage = styled.div<CheckProps>`
    position: relative;
    height: 300px;
    background-color: var(--light-blue-color);
    ${({isChosen}) => isChosen && css`
        border: 5px solid blue;
    `}
`

export const TagNumber = styled.span`
    position: absolute;
    top: 15px;
    left: 15px;
    font-size: 20px;
    color: white;
`