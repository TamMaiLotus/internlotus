import { useState } from 'react';
import { Image } from "@/components/Body/LandingPage/ContentLayout/CollectionView/NftItemsContainer/styled";
import { NftImage, TagNumber } from "./styled";

interface NftItemsLayoutProps {
  collectionItem: {
    id: number;
    image: string;
  };
  isChosen: boolean;
  onSelect: () => void;
}

export const NftItemsLayout: React.FunctionComponent<NftItemsLayoutProps> = ({collectionItem, isChosen, onSelect}) => {
  const [selected, setSelected] = useState(isChosen);

  const handleClick = () => {
    setSelected(!selected);
    onSelect();
  };

  return (
    <NftImage onClick={handleClick} isChosen={selected}>
      <Image src={collectionItem.image} alt="Kitten" />
      <TagNumber style={{ fontWeight: "bold" }}>#{collectionItem.id}</TagNumber>
    </NftImage>
  );
};
