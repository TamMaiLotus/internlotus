import styled from "styled-components";
import { ColumnFlexContainer } from "../../GlobalStyles/FlexContainer";

export const StyledSingleCollectionPage = styled(ColumnFlexContainer)`
    width: 100%;
`;