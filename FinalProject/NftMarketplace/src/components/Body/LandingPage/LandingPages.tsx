import { StyledLandingPage } from "./styled";
import { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import { Header } from "@/components/Headers/Header";
import { useSelector, useDispatch } from 'react-redux';
import { setSelectedItem } from '@/redux/selectedItemSlice';
import { RootState, AppDispatch } from '@/redux/store';

export const LandingPages = () => {
  const selectedItem = useSelector((state: RootState) => state.selectedItem.value);
  const dispatch: AppDispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(setSelectedItem(null));
  }, [location.pathname, dispatch]);

  return (
    <>
      <Header />
      <StyledLandingPage path={location.pathname}>
        <Outlet/>
      </StyledLandingPage>
    </>
  );
};
