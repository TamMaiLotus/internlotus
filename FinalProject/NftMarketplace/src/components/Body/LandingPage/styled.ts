import styled, {css} from "styled-components";
import { RowFlexContainer } from "../../GlobalStyles/FlexContainer";

export const StyledLandingPage = styled.div<{path: string}>`
    display: flex;
    flex-direction: row;
    color: var(--primary-color);

    height: 100vh;
    padding: 0 5rem;
    margin-top: 1rem;
    justify-content: center;
`;