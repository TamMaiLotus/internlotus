import { CartContainerSell } from "./CartContainer/CartContainerSell";
import { TitleBar } from "./ContentLayout/TitleBar/TitleBar";
import { CollectionView } from "./ContentLayout/CollectionView/CollectionView";

export const SellYourToken: React.FunctionComponent = () => {
  return (
    <>
        <CartContainerSell/>
        <div style={{flex: "4 0 0"}}>
            <TitleBar/>
            <CollectionView/>
        </div>
    </>
  );
};