import { TextContainer, Description, StyledCartContainer, Title, ContentContainer } from "./styled";
import { useState, useEffect } from "react";
import BuyToken from "@/components/NFTToken/BuyToken";
import useReadAllowance from "@/hooks/useReadAllowance";
import { BigNumber } from "ethers";
import ApproveAccount from "@/components/NFTToken/ApproveAccount";
import { useSelector } from 'react-redux';
import { RootState } from '@/redux/store';

export const CartContainerBuy = () => {
  const selectedItem = useSelector((state: RootState) => state.selectedItem.value);
  const [stateNft, setStateNft] = useState(null);
  const [price, setPrice] = useState('');
  const { result: allowance } = useReadAllowance();
  const token = JSON.parse(localStorage.getItem(`key${selectedItem}`));

  useEffect(() => {
    if (selectedItem === null) {
      setStateNft(null);
    } else
      if (!allowance.eq(BigNumber.from('0'))) {
        setStateNft('allowed');
        setPrice(token.formattedPrice);
      } else {
        setStateNft('not allowed');
        setPrice(token.formattedPrice);
      }
  }, [selectedItem]);

  return (
    <StyledCartContainer>
      <h3>Token Info</h3>
      {(stateNft === 'allowed') ? (
        <>
          <ContentContainer>
            <TextContainer>
              <Title>Id</Title>
              <Description>{selectedItem}</Description>
            </TextContainer>
            <TextContainer>
              <Title>Price</Title>
              <Description>{price} BCO</Description>
            </TextContainer>
          </ContentContainer>
          <BuyToken
            tokenId={selectedItem}
            allowance={allowance}
            price={price}
          />
        </>
      ) : (stateNft === 'not allowed') ? (
        <>
          <div>Your account is not approved for this marketplace yet.</div>
          <ApproveAccount />
        </>
        ) : (
        <div>Select one token to view the status of it.</div>)}
    </StyledCartContainer>
  );
};
