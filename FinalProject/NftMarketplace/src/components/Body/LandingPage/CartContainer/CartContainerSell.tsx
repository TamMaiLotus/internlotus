import { StyledCartContainer } from "./styled";
import { useSelector } from 'react-redux';
import { RootState } from '@/redux/store';
import { useState, useEffect } from "react";
import useGetApproved from "@/hooks/useGetApproved";
import useGetPrice from "@/hooks/useGetPrice";
import { Form } from "react-bootstrap";
import { ButtonContainer } from "@/components/popups/createNftModal/styled";
import PriceSell from "@/components/NFTToken/PriceSell";
import SellToken from "@/components/NFTToken/SellToken";
import ApproveToken from "@/components/NFTToken/ApproveToken";

const InvalidAddress = '0x0000000000000000000000000000000000000000'

export const CartContainerSell = () => {
  const selectedItem = useSelector((state: RootState) => state.selectedItem.value);
  const [stateNft, setStateNft] = useState('');
  const [price, setPrice] = useState('');
  const [priceState, setPriceState] = useState(false);
  const [sellState, setSellState] = useState(false);
  const { result: approvedAddress } = useGetApproved(selectedItem);
  const { result: orgPrice } = useGetPrice(selectedItem);
  useEffect(() => {
    if (selectedItem === null) {
      setStateNft('Not Selected');
    } else if (approvedAddress !== InvalidAddress) {
      setStateNft('Approved');
    } else {
      setStateNft('Not Approved');
    }
  }, [selectedItem, approvedAddress]);

  useEffect(() => {
    console.log(price);
    const checkPrice = ((price === orgPrice) || (price === '0') || (price === ''));
    setPriceState(checkPrice);
  }, [price, orgPrice]);

  const handleStatePrice = () => {
    setPrice('');
    setPriceState(false);
    setSellState(true);
  };

  const handleStateSell = () => {
    setSellState(false);
  }

  const handleValueChange = (e: any) => {
    setPrice(e.target.value);
    if (e.target.value === '') {
      setSellState(true);
      setPriceState(false);
    } else {
      setSellState(false);
      setPriceState(true);
    }
  }
  
  return (
    <StyledCartContainer>
      <h3>Token Info</h3>
      {(stateNft === 'Approved') ? (
        <div>
          <Form>
            <Form.Group controlId="formFile" className="mb-3">
              <Form.Label>Set Price</Form.Label>
              <Form.Control
                type="number"
                className="mb-4"
                step="0.1"
                size="lg"
                placeholder={orgPrice}
                value={price}
                onChange={e => handleValueChange(e)} />
              <ButtonContainer>
                <PriceSell id={selectedItem} price={price} state={priceState} onClick={handleStatePrice} />
                <SellToken id={selectedItem} state={sellState} onClick={handleStateSell} />
              </ButtonContainer>
            </Form.Group>

          </Form>
        </div>
      ) : (stateNft === 'Not Approved') ? (
        <>
          <span>It seem like you haven't approved this token yet</span>
          <ApproveToken id={selectedItem}>Approve</ApproveToken>
        </>
      ) : (<div>Select one token to view the status of it.</div>)}
    </StyledCartContainer>
  );
};