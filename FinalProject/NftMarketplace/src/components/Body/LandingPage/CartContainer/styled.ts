import styled from "styled-components";
import { ColumnFlexContainer } from "@/components/GlobalStyles/FlexContainer";

export const StyledCartContainer = styled(ColumnFlexContainer)`
    flex: 1 0 0;
    margin-right: 40px;
    margin-top: 50px;
    font-size: 16px;
    line-height: 1.5;
    gap: 30px
`

export const ContentContainer = styled(ColumnFlexContainer)`
    gap: 10px;
`

export const TextContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: baseline;
`

export const Title = styled.div`
    text-align: left;
    font-weight: 600;
    font-size: 18px;
`

export const Description = styled.div`
    text-align: right;
`

