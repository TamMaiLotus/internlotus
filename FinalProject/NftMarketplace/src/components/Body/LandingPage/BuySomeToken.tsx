import { CartContainerBuy } from "./CartContainer/CartContainerBuy";
import { TitleBar } from "./ContentLayout/TitleBar/TitleBar";
import { TokenSellingView } from "./ContentLayout/TokenSellingView/TokenSellingView";

export const BuySomeToken: React.FunctionComponent = () => {
  return (
    <>
        <CartContainerBuy/>
        <div style={{flex: "4 0 0"}}>
            <TitleBar/>
            <TokenSellingView/>
        </div>
    </>
  );
};