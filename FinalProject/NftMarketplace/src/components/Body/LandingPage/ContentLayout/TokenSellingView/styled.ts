import styled from "styled-components";
import { StyledCollectionView } from "../CollectionView/styled";
import { GridView } from "../../../../../components/GlobalStyles/GridContainer";

export const StyledTokenSellingView = styled(GridView)`
    grid-template-columns: repeat(4,minmax(0,1fr));
`

export const Title = styled.h3`
    font-size: 28px;
    font-weight: 500;
    text-transform: uppercase;
    color: var(--primary-color);
`

export const SellingCollections = styled(StyledCollectionView)`
    padding: 0px;
    margin: 0px;
`

