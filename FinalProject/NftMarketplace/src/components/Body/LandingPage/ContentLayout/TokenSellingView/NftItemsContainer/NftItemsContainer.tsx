import { StyledNftItemsContainer, NftImage, PriceTag, Image } from "./styled";
import { TagNumber } from "@/components/GlobalStyles/Title";

interface NftItemsContainerProps {
    token: any;
    name: string;
    isChosen: boolean;
    onSelect: () => void
}
export const NftItemsContainer: React.FunctionComponent<NftItemsContainerProps> = ({ token, name, isChosen, onSelect }) => {
    return (
        <StyledNftItemsContainer onClick={onSelect} isChosen={isChosen}>
            <NftImage>
                <Image src={token.tokenWithPrice.image} alt="Kitten" />
                <TagNumber style={{ fontWeight: "bold" }}>#{token.tokenWithPrice.id}</TagNumber>
            </NftImage>
            <PriceTag>
                <div style={{fontWeight: 'bold', textAlign: "left"}}>PRICE: </div>
                <div style={{textAlign: "right"}}>{token.tokenWithPrice.formattedPrice} {name}</div>
            </PriceTag>
        </StyledNftItemsContainer>
    )
};