import { NftItemsContainer } from "../CollectionView/NftItemsContainer/NftItemsContainer";
import { StyledTokenSellingView } from "./styled";
import useAllHoldings from "@/hooks/useAllHoldings";
import useGetPrices from "@/hooks/useGetPrices";
import { useReadBCO } from "@/hooks/useReadBCO";
import { useSelector, useDispatch } from 'react-redux';
import { setSelectedItem } from '@/redux/selectedItemSlice';
import { RootState, AppDispatch } from '@/redux/store';
import { Loader } from "@/components/Loader/Loader";
import { ErrorMessage } from "@/components/ErrorMessage/ErrorMessage";

export const TokenSellingView = () => {
  const { result: tokens, isLoading: ahLoading, error: ahError } = useAllHoldings();
  const { result: tokensWithPrice, isLoading: gpLoading, error: gpError } = useGetPrices(tokens);
  const ERC20Name = useReadBCO();
  const selectedItem = useSelector((state: RootState) => state.selectedItem.value);
  const dispatch: AppDispatch = useDispatch();

  if (ahLoading || gpLoading) {
    return <Loader />
  }

  if (ahError) {
    return <ErrorMessage place="useAllHoldings" message={ahError}/>
  }

  if (gpError) {
    return <ErrorMessage place="useGetPrices" message={gpError}/>
  }

  return (
    <StyledTokenSellingView>
      {tokensWithPrice.map((token) => (
        <NftItemsContainer
          key={token.tokenWithPrice.id}
          token={token}
          name={ERC20Name}
          isChosen={token.tokenWithPrice.id === selectedItem}
          onSelect={() => dispatch(setSelectedItem(token.tokenWithPrice.id === selectedItem ? null : token.tokenWithPrice.id))} />
      ))}
    </StyledTokenSellingView>
  );
};
