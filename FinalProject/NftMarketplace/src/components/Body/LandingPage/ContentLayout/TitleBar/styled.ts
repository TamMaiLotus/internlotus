import styled from "styled-components";
import { RowFlexContainer } from "../../../../GlobalStyles/FlexContainer";
import { Form, FormControl, InputGroup } from 'react-bootstrap'

export const StyledTitleBar = styled(RowFlexContainer)`
    justify-content: flex-start;
    align-items: center;
    gap: 20px;
    height: 5rem;
`

export const Button = styled.button`
    padding: 0 30px;
    font-size: 18px;
    text-align: center;
    text-decoration: none;
    line-height: 3;
    border-radius: 18px;
    flex: 1 1 0;
`

export const BuyTokenButton = styled(Button)`
    background-color: var(--secondary-color);
    color: black;
    &:disabled {
        background-color: #A8EAFF;
        color: #969696;
    }
`

export const SellTokenButton = styled(Button)`
    background-color: var(--primary-bold-color);
    color: white;
`

export const FilterSelect = styled(Form.Select)`
    border-style: none;
    border-radius: 0;
    border-bottom: 1.5px solid var(--primary-color);

    color: var(--primary-color);
    font-size: 20px;

    flex: 1 1 auto;
`

export const SearchBar = styled(InputGroup)`
    flex: 1 1 30%;
`

export const InputSearch = styled(FormControl)`
    color: var(--primary-color);
    font-size: 20px;
`