import { AppRouters } from "@/constants/AppRoutes";
import { StyledTitleBar, BuyTokenButton, SellTokenButton, FilterSelect, SearchBar, InputSearch } from "./styled";
import { InputGroup } from "react-bootstrap";
import { BiSearch } from 'react-icons/bi'
import { useNavigate } from "react-router-dom";

export const TitleBar = () => {
  const navigate = useNavigate();

  return (
    <StyledTitleBar>
      <h1>Browse Tokens</h1>
      <div style={{flex: "2 1 0"}}/>
      <SellTokenButton onClick={() => navigate(AppRouters.SELL)}>Sell your Tokens</SellTokenButton>
      <BuyTokenButton onClick={() => navigate(AppRouters.BUY)}>Buy some Tokens</BuyTokenButton>
    </StyledTitleBar>
  );
};