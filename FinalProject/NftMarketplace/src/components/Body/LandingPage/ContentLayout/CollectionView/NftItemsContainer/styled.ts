import styled, {css} from "styled-components";
import { ColumnFlexContainer } from "../../../../../GlobalStyles/FlexContainer";

interface CheckProps {
    isChosen: boolean;
}

export const StyledNftItemsContainer = styled(ColumnFlexContainer)<CheckProps>`
    gap: 20px;
    ${({isChosen}) => isChosen && css`
        border: 5px solid blue;
    `}
`

export const NftImage = styled.div`
    position: relative;
    height: 300px;
    background-color: var(--light-blue-color);
    flex: 0 1 auto;

`
export const Image = styled.img`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
`

export const PriceTag = styled.div`
    display: flex;
    justify-content: space-between;
    font-size: 20px;
`