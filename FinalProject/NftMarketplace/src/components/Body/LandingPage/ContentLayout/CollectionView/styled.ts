import styled from "styled-components";
import { GridView } from "@/components/GlobalStyles/GridContainer";

export const StyledCollectionView = styled(GridView)`
    grid-template-columns: repeat(4,minmax(0,1fr));
`;