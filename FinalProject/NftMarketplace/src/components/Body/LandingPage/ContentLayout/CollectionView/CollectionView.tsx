import { StyledCollectionView } from "./styled";
import { useAccount } from "wagmi";
import { useFilterNFTs } from "@/hooks/useFilterNFTs";
import { NftItemsLayout } from "@/components/Body/SingleCollectionPage/CollectionView/NftItemsLayout/NftItemsLayout";
import { useSelector, useDispatch } from 'react-redux';
import { setSelectedItem } from '@/redux/selectedItemSlice';
import { RootState, AppDispatch } from '@/redux/store';
import { Loader } from "@/components/Loader/Loader";
import { ErrorMessage } from "@/components/ErrorMessage/ErrorMessage";

export const CollectionView = () => {
  const { address } = useAccount();
  const {nfts, isLoading, error} = useFilterNFTs(address);
  const selectedItem = useSelector((state: RootState) => state.selectedItem.value);
  const dispatch: AppDispatch = useDispatch();

  if (isLoading) 
    return <Loader/>
  
  if (error)
    return <ErrorMessage place="useFilterNFTs" message={error}/>

  return (
    <StyledCollectionView>
      {(nfts) ? (nfts.filter(item => item && item.id !== null).map((item) => (
        <NftItemsLayout
          key={item.id}
          collectionItem={item}
          isChosen={item.id === selectedItem}
          onSelect={() => dispatch(setSelectedItem(item.id === selectedItem ? null : item.id))} />
      ))
      ) : (
        <></>
      )}
    </StyledCollectionView>
  );
};
