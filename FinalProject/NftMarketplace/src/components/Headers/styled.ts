import styled, { css } from 'styled-components'
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import { Nav } from 'react-bootstrap';


export const CustomNavbar = styled(Navbar)`
    width: 100vw;
    overflow-x: hidden;
    padding: 0 5rem !important;
    height: 8rem;
    ${props => props.path === "/" && css`
        display: none;`
    }
`

export const Link = styled(Nav)`
    align-items: center;
    flex-direction: row;
    column-gap: 3em;
`

export const ConnectButton = styled(Button)`
    width: 8em;
    height: 4em;
    border-radius: 15px;
    background-color: #1D4ED8 !important;
`