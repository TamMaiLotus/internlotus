import { Navbar } from 'react-bootstrap'
import { ConnectButton, CustomNavbar, Link } from "./styled"
import { useAccount, useDisconnect } from 'wagmi';
import { useState } from 'react';
import { ConnectWallet } from '../popups/createWalletModal/ConnectWalletModal';
import { NftModalCreation } from '../popups/createNftModal/CreateNftModal';
import { useLocation, useNavigate } from 'react-router-dom';
import { AppRouters } from '@/constants/AppRoutes';

export const Header = () => {
    const { address: account, isConnected } = useAccount()
    const [openModal, setOpenModal] = useState('')
    const { disconnect } = useDisconnect()
    const location = useLocation();
    const navigate = useNavigate();
    const accountId = '/' + account;
    return (
        <CustomNavbar path={location.pathname} data-bs-theme="light" expand="lg">
            <Navbar.Brand href={'/'}>
                <h2 style={{ color: "#2563EB" }}>NFT Marketplace</h2>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="h-100">
                <Link className="ms-auto">
                    <a style={{ textDecoration: "none" }}>
                        {/* <w3m-button size="md" loadingLabel='Connecting...'/> */}
                        {isConnected ? (
                            <ConnectButton onClick={() => {
                                navigate(AppRouters.HOME);
                                disconnect();
                               }}
                                >{account.substring(0,4)}...{account.slice(-4)}</ConnectButton>
                        ) : (
                            <ConnectButton onClick={() => setOpenModal('Connect')}>Connect</ConnectButton>
                        )}
                    </a>
                    <a 
                        style={{ textDecoration: "none" }} 
                        onClick={() => setOpenModal('Create')} 
                        className="custom-navLink">
                            Create your NFT
                    </a>
                    <a
                        style={{ textDecoration: "none" }}
                        href={'/account' + accountId}
                        className="custom-navLink">
                            Account
                    </a>
                </Link>
            </Navbar.Collapse>
            {openModal === 'Connect' && <ConnectWallet setOpenModal={setOpenModal} />}
            {openModal === 'Create' && <NftModalCreation setOpenModal={setOpenModal} account={account}/>}
        </CustomNavbar>

    )
}