import { JsonRpcSigner } from '@ethersproject/providers'
import { useVaultContract } from '@/hooks/useVaultContract'
import { useNetwork, useSigner } from 'wagmi'
import { useState } from 'react';
import { PriceSellButton } from './styled';

interface SellTokenProps {
  id: number;
  state: boolean;
  onClick: () => void;
}

const SellToken: React.FC<SellTokenProps> = ({ id, state, onClick }) => {
  const [isLoading, setIsLoading] = useState(false);
  const { chain } = useNetwork();
  const { data: signer } = useSigner();

  const { sellToken } = useVaultContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await sellToken(id);
      console.log(tx);
      alert('Approve success');
      onClick();
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  const testSelling = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await sellToken(id);
      console.log(tx);
      alert('Approve success');
      onClick();
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  return (
    // <PriceSellButton
    //   onClick={handleClickEvent}
    //   disabled={isLoading || !state}>
    //   {isLoading ? 'Selling...' : 'Sell'}
    // </PriceSellButton>
    <PriceSellButton
    onClick={testSelling}>
      Sell
  </PriceSellButton>
  )
}

export default SellToken