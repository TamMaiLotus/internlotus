import { JsonRpcSigner } from '@ethersproject/providers'
import { useBCOContract } from '@/hooks/useBCOContract'
import { useAccount, useNetwork, useSigner } from 'wagmi'
import { useState } from 'react';
import { ApproveButton } from './styled';

const ApproveAccount = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { chain } = useNetwork();
  const { data: signer } = useSigner();
  const {address} = useAccount();
 
  const { approve } = useBCOContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await approve(address);
      console.log(tx);
      alert('Approve success');
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  return (
    <ApproveButton
      onClick={handleClickEvent}
      disabled={isLoading}>
      {isLoading ? 'Approving...' : 'Approve'}
    </ApproveButton>
  )
}

export default ApproveAccount