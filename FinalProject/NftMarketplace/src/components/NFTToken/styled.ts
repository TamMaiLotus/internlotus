import styled from "styled-components";

export const MintButton = styled.button`
    background-color: var(--primary-color);
    border-radius: 5px;
    width: 60%;
    flex: 1 0 auto;
`

export const PriceSellButton = styled.button`
    background-color: var(--primary-color);
    border-radius: 15px;
    flex: 1 1 auto;
`

export const ApproveButton = styled.button`
    background-color: var(--primary-color);
    border-radius: 15px;
    font-size: 20px;
`