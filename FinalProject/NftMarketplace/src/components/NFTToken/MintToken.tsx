import { JsonRpcSigner } from '@ethersproject/providers'
import { useNFTContract } from '@/hooks/useNFTContract'
import { useNetwork, useSigner } from 'wagmi'
import { useState, useEffect } from 'react';
import { MintButton } from './styled';

interface MintTokenProps {
  to: string;
  img: any;
  src: string;
}

const MintToken: React.FC<MintTokenProps> = ({ to, img, src }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [tokenId, setTokenId] = useState(null);
  const { chain } = useNetwork();
  const { data: signer } = useSigner();

  useEffect(() => {
    if (img !== null) {
      const fileName = img.name as string;
      let start = fileName.lastIndexOf('_') + 1;
      let end = fileName.lastIndexOf('.');
      const baseName = fileName.substring(start, end);
      setTokenId(parseInt(baseName));
    }
  }, [img]);

  const { mint } = useNFTContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleCreate = () => {
    console.log(img)
    let value = {
      image: src,
      id: tokenId,
      orgOwner: to,
      curOwner: to,
    }
    let jsonValue = JSON.stringify(value);
    localStorage.setItem(`key${tokenId}`, jsonValue)

    var storedValue = localStorage.getItem(`key${tokenId}`);
    if (storedValue === jsonValue) {
      console.log('Item was successfully set in localStorage');
    } else {
      console.log('Failed to set item in localStorage');
    }
    let parsedValue = JSON.parse(jsonValue);
    console.log(parsedValue.image);
  }

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await mint(to, tokenId);
      console.log(tx);
      handleCreate();
      alert('Mint success');
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  
  return (
    <MintButton
      onClick={handleClickEvent}
      disabled={isLoading}>
      {isLoading ? 'Minting...' : 'Mint'}
    </MintButton>
    // <button onClick={handleCreate}>Create</button>
  )
}

export default MintToken