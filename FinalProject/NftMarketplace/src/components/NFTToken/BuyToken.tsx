import { JsonRpcSigner } from '@ethersproject/providers'
import { useNetwork, useSigner } from 'wagmi'
import { useVaultContract } from '@/hooks/useVaultContract'
import { ApproveButton } from './styled'
import { useState } from 'react'
import { BigNumber } from 'ethers'
import { parseEther } from 'ethers/lib/utils.js'

interface MintTokenProps {
  tokenId: number,
  allowance: BigNumber,
  price: string,
}

const BuyToken: React.FC<MintTokenProps> = ({ tokenId, allowance, price }) => {
  const { chain } = useNetwork();
  const { data: signer } = useSigner();
  const [isLoading, setIsLoading] = useState(false);

  const { buyToken } = useVaultContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await buyToken(tokenId);
      console.log(tx);
      alert('Buy success');
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }

  console.log(parseEther(price));

  return (
    <>
      <ApproveButton
        onClick={handleClickEvent}
        disabled={isLoading || (allowance.lt(parseEther(price)))}>
        {isLoading ? 'Buying...' : 'Buy Token'}
      </ApproveButton>
      {(allowance.lt(parseEther(price))) && <div>Your allowance is less than the price</div>}
    </>
  )
}

export default BuyToken