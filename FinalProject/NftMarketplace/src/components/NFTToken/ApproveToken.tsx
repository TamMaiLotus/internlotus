import { JsonRpcSigner } from '@ethersproject/providers'
import { useNFTContract } from '@/hooks/useNFTContract'
import { useNetwork, useSigner } from 'wagmi'
import { useState } from 'react';
import { ApproveButton } from './styled';

interface ApproveTokenProps {
  id: number;
  children: React.ReactNode;
}

const ApproveToken: React.FC<ApproveTokenProps> = ({ id }) => {
  const [isLoading, setIsLoading] = useState(false);
  const { chain } = useNetwork();
  const { data: signer } = useSigner();

  const { approve } = useNFTContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const tx = await approve('0xB37f99CAD2B7Dc870A2E4e385cbA1AD2E759Fd50', id);
      console.log(tx);
      alert('Approve success');
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  return (
    <ApproveButton
      onClick={handleClickEvent}
      disabled={isLoading}>
      {isLoading ? 'Approving...' : 'Approve'}
    </ApproveButton>
    // <button onClick={handleCreate}>Create</button>
  )
}

export default ApproveToken