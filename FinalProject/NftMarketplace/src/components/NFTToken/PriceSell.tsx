import { JsonRpcSigner } from '@ethersproject/providers'
import { useVaultContract } from '@/hooks/useVaultContract'
import { useNetwork, useSigner } from 'wagmi'
import { useState } from 'react';
import { ethers } from 'ethers';
import { PriceSellButton } from './styled';

interface PriceSellProps {
  id: number;
  price: string;
  state: boolean;
  onClick: () => void;
}

const PriceSell: React.FC<PriceSellProps> = ({ id, price, state, onClick }) => {
  const [isLoading, setIsLoading] = useState(false);
  const { chain } = useNetwork();
  const { data: signer } = useSigner();

  const { setPrice, allHoldings } = useVaultContract(
    chain.id,
    signer as JsonRpcSigner,
  )

  const handleClickEvent = async (e: any) => {
    try {
      e.preventDefault();
      setIsLoading(true);
      const convertedPrice = ethers.utils.parseUnits(price, "ether");
      const tx = await setPrice(id, convertedPrice.toBigInt());
      const allHolding = await allHoldings();
      console.log('holding', allHolding);
      alert('Set Price success');
      onClick();
    }
    catch (e: any) {
      console.error(e)
      alert(`Error: ${e.message}`);
    }
    finally {
      setIsLoading(false);
    }
  }
  return (
    <PriceSellButton
      onClick={handleClickEvent}
      disabled={isLoading || state}>
      {isLoading ? 'Setting price...' : 'Set Price'}
    </PriceSellButton>
  )
}

export default PriceSell