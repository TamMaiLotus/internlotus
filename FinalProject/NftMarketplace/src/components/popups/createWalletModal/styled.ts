import styled from "styled-components";

export const ModalContainer = styled.div`
    max-width: 500px;
    width: 100%;
    position: fixed;
    top: 50%;
    left: 50%;
    padding: 50px;
    transform: translate(-50%, -50%);
    z-index: 9999;
    display: flex;
    flex-direction: column;
    row-gap: 15px;
    background-color: #ffffff;
    box-shadow: 0px 0px 18px 0px rgba(0, 0, 0, 0.75);
    border-radius: 8px;     
    color: var(--primary-color);
`
export const Title = styled.h1`
    color: var(--primary-bold-color);
    font-weight: 600;
    margin-bottom: 10px;
`
export const Button = styled.button`
    border-radius: 15px;
    background-color: var(--primary-bold-color);
`