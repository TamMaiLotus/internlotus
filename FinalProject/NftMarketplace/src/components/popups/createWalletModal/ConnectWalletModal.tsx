import { DimBackground } from "../createNftModal/styled";
import { Button, ModalContainer, Title } from "./styled";
import { useConnect } from 'wagmi'

interface NftModalProps {
    setOpenModal: React.Dispatch<React.SetStateAction<string>>;
}

export const ConnectWallet: React.FC<NftModalProps> = ({ setOpenModal }) => {
    const { connect, connectors, error, isLoading, pendingConnector } =useConnect()
    return (
        <DimBackground>
            <ModalContainer onClick={() => setOpenModal('')}>
                <Title>Connect to Wallet</Title>
                {connectors.map((connector) => (
                    <Button
                        disabled={!connector.ready}
                        key={connector.id}
                        onClick={() => connect({connector})}
                    >
                        {connector.name}
                        {!connector.ready && ' (unsupported)'}
                        {isLoading &&
                            connector.id === pendingConnector?.id &&
                            ' (connecting)'}
                    </Button>
                ))}

                {error && <div>{error.message}</div>}
            </ModalContainer>
        </DimBackground>
    )
}