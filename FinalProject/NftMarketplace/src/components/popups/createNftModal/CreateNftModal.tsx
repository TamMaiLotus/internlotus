import { Form } from "react-bootstrap";
import { useState } from "react";
import { Image, ButtonContainer, CancelButton, DimBackground, ImageContainer, ModalContainer, ModalContentContainer, Title} from "./styled";
import MintToken from "@/components/NFTToken/MintToken";

interface NftModalProps {
    setOpenModal: React.Dispatch<React.SetStateAction<string>>,
    account: string,
}

export const NftModalCreation: React.FC<NftModalProps> = ({ setOpenModal, account }) => {
    const [image, setImage] = useState(null);
    const [imgData, setImgData] = useState(null);
    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files[0]) {
            setImgData(e.target.files[0]);
            const reader = new FileReader();
            reader.onloadend = () => {
                setImage(reader.result);
            };
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    return (
        <DimBackground>
            <ModalContainer>
                <Title>Create your NFT...</Title>
                <ModalContentContainer>
                    <ImageContainer>
                        {image && <Image src={image} alt="preview" />}
                    </ImageContainer>
                    <Form>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Upload your photo</Form.Label>
                            <Form.Control type="file"
                                onChange={handleFileChange} />
                        </Form.Group>
                        <ButtonContainer>
                            <CancelButton onClick={() => setOpenModal('')}>Cancel</CancelButton>
                            <MintToken to={account} img={imgData} src={image}/>
                        </ButtonContainer>
                    </Form>
                </ModalContentContainer>
            </ModalContainer>
        </DimBackground>
    )
}