import styled from "styled-components";

export const DimBackground = styled.div`
    background-color: rgba(0,0,0,0.5);
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 10;
    position: fixed;
`

export const ModalContainer = styled.div`
    max-width: 900px;
    width: 100%;
    position: fixed;
    top: 50%;
    left: 50%;
    padding: 50px;
    transform: translate(-50%, -50%);
    z-index: 9999;

    background-color: #ffffff;
    box-shadow: 0px 0px 18px 0px rgba(0, 0, 0, 0.75);
    border-radius: 8px;     
`

export const Title = styled.h1`
    color: var(--primary-color);
    font-weight: 600;
    margin-bottom: 30px;
`

export const ModalContentContainer = styled.div`
    display: flex;
    flex-direction: row;
    column-gap: 10px;
    justify-content: space-between;
    position: relative;
`

export const ImageContainer = styled.div`
    width: 45%;
    background-color: var(--background-blue-color);
    height: 300px;
`

export const ButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: 20px;
    gap: 10px;
`

export const CancelButton = styled.button`
    background-color: transparent;
    color: var(--primary-color);
    flex: 1 0 auto;
`

export const AcceptButton = styled.button`
    background-color: var(--primary-color);
    border-radius: 5px;
    width: 60%;
    flex: 0 0 auto;
`

export const Image = styled.img`
    width: 100%;
    height: 100%;
`