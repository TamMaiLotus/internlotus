import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface SelectedItemState {
  value: number | null;
}

const initialState: SelectedItemState = {
  value: null,
};

export const selectedItemSlice = createSlice({
  name: 'selectedItem',
  initialState,
  reducers: {
    setSelectedItem: (state, action: PayloadAction<number | null>) => {
      state.value = action.payload;
    },
  },
});

export const { setSelectedItem } = selectedItemSlice.actions;

export default selectedItemSlice.reducer;
