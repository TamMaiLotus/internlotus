import { AppRouters } from '../constants/AppRoutes';
import { ReactElement } from 'react';
import { Navigate } from 'react-router-dom';
import { useAccount } from 'wagmi';

export const PrivateRoute = ({
    children,
    } : {
    children: JSX.Element;
    }) : ReactElement | null => {
    const {address, isConnected} = useAccount();
    return address && isConnected ? children : <Navigate to={AppRouters.HOME} />;
};