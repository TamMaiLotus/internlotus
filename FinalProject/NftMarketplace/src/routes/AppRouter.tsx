// import { LoaderIcon } from 'components/Loader/LoaderIcon';
import { AppRouters } from '../constants/AppRoutes';
import { Suspense } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { Redirect } from './Redirect';
import { AccountPage } from '../components/Body/SingleCollectionPage/AccountPage';
import { LandingPages } from '../components/Body/LandingPage/LandingPages';
import { BuySomeToken } from '../components/Body/LandingPage/BuySomeToken';
import { SellYourToken } from '../components/Body/LandingPage/SellYourToken';
import { Dashboard } from '../components/Body/Dashboard/Dashboard';
import { Loader } from '../components/Loader/Loader';

export const AppRouter = () => {
  return (
    <Router>
      <Suspense fallback={<Loader />}>
        <Routes>
          <Route path="*" element={<Redirect to="/" />} />
          <Route path={AppRouters.HOME} element={<LandingPages />}>
            <Route index element={<Dashboard />} />
            <Route path={AppRouters.BUY} element={
              <PrivateRoute>
                <BuySomeToken />
              </PrivateRoute>} />
            <Route path={AppRouters.SELL} element={
              <PrivateRoute>
                <SellYourToken />
              </PrivateRoute>} />
              <Route path={AppRouters.ACCOUNT} element={
            <PrivateRoute>
              <AccountPage />
            </PrivateRoute>} />
          </Route>

        </Routes>
      </Suspense>
    </Router>
  );
};