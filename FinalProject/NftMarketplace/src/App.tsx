import './App.css'
import { AppRouter } from '@/routes/AppRouter'
import { WagmiConfig, createClient, configureChains } from 'wagmi'
import { mainnet, sepolia } from 'wagmi/chains'

import { jsonRpcProvider } from "wagmi/providers/jsonRpc";
import { publicProvider } from 'wagmi/providers/public'

import { CoinbaseWalletConnector } from 'wagmi/connectors/coinbaseWallet'
import { InjectedConnector } from 'wagmi/connectors/injected'
import { MetaMaskConnector } from 'wagmi/connectors/metaMask'
import { WalletConnectConnector } from 'wagmi/connectors/walletConnect'

import { Provider } from 'react-redux';
import { store } from '@/redux/store';

// Configure chains & providers with the Alchemy provider.
// Two popular providers are Alchemy (alchemy.com) and Infura (infura.io)
// 
const { chains, provider, webSocketProvider } = configureChains(
  [mainnet, sepolia],
  [jsonRpcProvider({
    rpc: chain => {
      if (chain.id === 42) {
        return {
          http: 'https://eth-sepolia.alchemyapi.io/v2/KGnvfCghFu8rrMOYGg8Xe0EEvn9VUPEC',
        };
      }
      return null;
    },
  }),
  publicProvider(),
  ],
)

// Set up wagmi config
const config = createClient({
  autoConnect: true,
  connectors: [
    new MetaMaskConnector({ chains }),
    new CoinbaseWalletConnector({
      chains,
      options: {
        appName: 'wagmi',
      },
    }),
    new WalletConnectConnector({
      chains,
      options: {
        projectId: '81b4a72f9cc2d79fa3e720291e06af1f',
      },
    }),
    new InjectedConnector({
      chains,
      options: {
        name: 'Injected',
        shimDisconnect: true,
      },
    }),
  ],
  provider,
  webSocketProvider,
})

function App() {
  return (
    <WagmiConfig client={config}>
      <Provider store={store}>
        <AppRouter />
      </Provider>
    </WagmiConfig>
  )
}

export default App
