import { createBrowserRouter } from 'react-router-dom';
import Index from "../pages/index";
import Root, {
    loader as rootLoader,
    action as rootAction
} from '../pages/root';
import ErrorPage from '../errorPage';
import Contact, {
    loader as contactLoader,
    action as contactAction,
} from '../pages/contact';
import EditContact, {
    action as editAction,
} from '../pages/edit';
import {
    action as destroyAction
} from "../pages/destroy"

//Initializing routes
const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        errorElement: <ErrorPage />,
        loader: rootLoader,
        action: rootAction,
        children: [{
            errorElement: <ErrorPage />,
            children: [
                {   index: true, element: <Index /> },
                {
                    path: "contacts/:contactId",
                    element: <Contact />,
                    loader: contactLoader,
                    action: contactAction,
                },
                {
                    path: "contacts/:contactId/edit",
                    element: <EditContact />,
                    loader: contactLoader,
                    action: editAction,
                },
                {
                    path: "contacts/:contactId/destroy",
                    action: destroyAction,
                    errorElement: <div>Oops, there's error.</div>
                }]
        }]
    },

]);

export { router }