# Blockchain: 
## 1. Khái niệm
* "Block" là các block lưu trữ dữ liệu và trạng thái trong các block liên tiếp.
* "Chain" là cầu nối tham chiếu mỗi block con sang các block parent thông qua các đoạn mã. 
* "Blockchain" Là hệ thống cơ sở dữ liệu cho phép lưu trữ và truyền tải các khối thông tin. Chúng được liên kết nhau nhờ mã hoá.
* "Node" là các nút giúp lưu trữ, truyền tải và bảo quản dữ liệu blockchain (PC, laptop, máy chủ, ...)
* Dữ liệu ở mỗi block ko thể thay đổi nếu như ko thay đổi dữ liệu các block xung quanh nó.
* Các khối thông tin hoạt động độc lập mà có thể mở rộng theo thời gian, được quản lí bởi những người tham gia hệ thống.

## 2. Cách hoạt động
* Blockchain hoạt động dựa trên nguyên tắc của mạng P2P (Peer to Peer - ko có máy chủ trung tâm, chỉ có sự đồng thuận giữa các node)
* Để một block – khối thông tin được thêm vào Blockchain, phải có 4 yếu tố:
  * Phải có giao dịch
  * Giao dịch đó phải được xác minh
  * Giao dịch đó phải được lưu trữ trong block
  * Block đó phải nhận được hash
* "Hash" là một mã string gồm các chữ cái và số, dưới dạng một "fingerprint" của một data. Khi data thay đổi thì hash này cũng thay đổi theo, nhưng mỗi hash sẽ được cố định với một data cố định.
* "Nonce" là một số mà người dùng thiết lập để cố tìm kiếm số nào phù hợp để Hash có thể bắt đầu với 4 số 0
* "Assigned Block" là một block mà mã Hash bắt đầu bằng 4 số 0
* "Blockchain" là các block kết nối với nhau qua các mã Hash, tức mã Hash prev của một block này là mã Hash của block trước nó. Nếu một block bị invalid với mã Hash của nó, các block kế tiếp của block đấy cũng sẽ bị ảnh hưởng theo, và ảnh hưởng tới cả 1 chain (Immutable). Thêm nữa, rất khó để mà chỉnh sửa cả chain này bởi vì chúng ta phải chỉnh sửa lại nonce của từng block trong chain bị ảnh hưởng. Các người dùng khác đểu có thể check xem một blockchain có bị validate hay ko một cách dễ dàng (Distributed).
* "Token" là dạng tài sản kĩ thuật số
* "Coinbase" là tiền được đưa cho người dùng nào đó
* Nếu người dùng nào cố gắng thay đổi tiền để trục lợi bản thân, lập tức block chứa nội dung đấy sẽ bị mark là invalid, và các người dùng khác đều có thể nhận thấy được block của người dùng đó bị invalid

# Smart Contact
## 1. Khái niệm
* Smart Contract là một chương trình tính toán được xây dựng trên nền tảng blockchain, được thiết kế để thực hiện các thỏa thuận và giao dịch. Smart Contract có thể được hiểu là một loại hợp đồng số, tự động hóa các thỏa thuận giữa các bên, không cần đến bên trung gian để thi hành (Được truy cập và minh bạch).
* Bất kì dev nào đều có thể tạo ra hợp đồng thông minh và đưa chúng vào mạng, sử dụng blockchain như lớp dữ liệu của chúng, và trả phí mạng cho các ISP. Do đó, với smart contact, các dev đều có thể xây dựng và triển khai ứng dụng và dịch vụ bất kỳ, chẳng hạn như: sàn giao dịch, công cụ tài chính, trò chơi, v.v...

## 2. Các tính năng chính
* Tự động hóa các thỏa thuận và giao dịch. 
* Được xây dựng trên nền tảng blockchain, đảm bảo tính toàn vẹn và an toàn dữ liệu. 
* Không cần đến bên trung gian để thi hành, giảm thiểu chi phí và thời gian cho các bên. 
* Cung cấp môi trường minh bạch và công khai cho các giao dịch.

# Etherium
## 1. Khái niệm
* Ethereum là một blockchain được tích hợp máy tính. Đó là nền tảng để xây dựng ứng dụng và tổ chức một cách phi tập trung, không cần phê duyệt, và chống lại sự kiểm duyệt.
* Ethereum là một "không gian mạng" cho các máy tính trên toàn thế giới, cùng tuân theo một loạt quy tắc gọi là giao thức Etherum. Bất kì ai cũng có thể tham gia sử dụng và đóng góp không giới hạn.

## 2. Lợi ích
- Ai cũng có thể tham gia
- Giao dịch toàn cầu nhanh chóng và dễ dàng hơn, kể cả khi đất nước người dùng đang trong thời kì khủng hoảng
- Khai thác nguồn content mới dành cho các creator
- "Play to earn"

## 3. Cách hoạt động
* Ethereum sử dụng cơ chế đồng thuận PoS.
    (**PoS**: Là một thuật toán để chứng minh rằng người xác thực đã đặt cược một thứ có giá trị trên network, và thứ đấy có thể bị phá huỷ nếu họ vi phạm điều luật của smart contact)
* Bất kì ai muốn thêm khối mới vào chuỗi phải đặt cược ETH - đồng tiền gốc trong Etherum - như một tài sản thế chấp, rồi chạy phần mềm xác thực. Những người xác thực này sau đó sẽ được chọn ngẫu nhiên để đề xuất các block mà người khác có thể kiểm tra và thêm vào blockchain.
* Cơ chế đồng thuận này đảm bảo rằng sau khi các giao dịch được xác minh là hợp lệ và được thêm vào blockchain, chúng không thể bị can thiệp sau này. Đồng thời, cơ chế này cũng đảm bảo rằng tất cả các giao dịch được ký và thực thi với "quyền hạn" thích hợp

# DApps
## 1. Khái niệm
* DApp - Decentralized Application: là một ứng dụng phi tập trung được xây dựng dựa trên mạng lưới phi tập trung kết hợp giữa smart contact và front-end UI. 
* Một dapp có thể có code cho front-end và UI viết bởi bất kì loại ngôn ngữ nào để gọi tới back-end. Ngoài ra, front-end đó có thể được host với các host phi tập trung như [IPFS](https://ipfs.tech/)
* DApp có các đặc trưng riêng là: mã nguồn mở, decentralized (đảm bảo sự tin cậy, hiệu quả và minh bạch), khuyến khích (người dùng dc lượng cryto nhất định khi mà hoạt động trên blockchain)
* DApp có thể phi tập trung nhờ vào việc quản lí các logic được viết trong các smart contract.
## 2. Lợi ích và tác hại
**Lợi ích:** Zero downtime, sự riêng tư, chống sự kiểm duyệt, toàn vẹn dữ liệu, không cần sự tin tưởng vào một bên nào cả
**Tác hại:** Bảo trì khó khăn hơn, chi phí hiệu năng lớn (Scale khó khăn), tắc nghẽn hệ thống mạng, khó khăn trong thiết kế UX, và sự dựa dẫm vào hệ thống tập trung
